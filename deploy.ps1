$ver = $Args[0]

$verText = $ver.replace(".", "")
$targetDir = ".\dist_electron\Data_Warehouse\catovis${verText}"

if ((Test-Path $targetDir) -eq $false) {
    mkdir $targetDir
}

Copy-Item -Force CATOVIS_WordCore.dotm  $targetDir
Copy-Item -Force UpdateLog.txt $targetDir
Copy-Item -Force config.json $targetDir
Copy-Item -Force TemplateSet.ps1 $targetDir
Copy-Item -Force ".\dist_electron\catovis Setup ${ver}.exe" $targetDir

Compress-Archive -Force -path $targetDir -DestinationPath "$targetDir.zip"

Write-Host "CATOVIS ${ver}.zip has been created successfully !"

Read-Host("Please type any key:")