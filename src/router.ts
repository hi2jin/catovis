import Vue from 'vue';
import Router from 'vue-router';
import catPage from './views/catPage.vue';
import filterPage from './views/filterPage.vue';
// import checkPage from './views/checkPage.vue';
import prefPage from './views/prefPage.vue';
import prefMT from './views/prefMT.vue';
import dbManager from './views/dbManager.vue';
import standalonePage from './views/standalonePage.vue';
import termHelper from './views/termHelper.vue';
import importWizardPage from './views/importWizardPage.vue';
import exportWizardPage from './views/exportWizardPage.vue';
import restorePage from './views/restorePage.vue';
import testPage from './views/testPage.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/cat',
      name: 'catPage',
      component: catPage,
    },
    {
      path: '/filter',
      name: 'filterPage',
      component: filterPage,
    },
    // {
    //   path: '/check',
    //   name: 'checkPage',
    //   component: checkPage,
    // },
    {
      path: '/pref',
      name: 'prefPage',
      component: prefPage,
    },
    {
      path: '/prefMT',
      name: 'prefMT',
      component: prefMT,
    },
    {
      path: '/dbManager',
      name: 'dbManager',
      component: dbManager,
    },
    {
      path: '/termhelper',
      name: 'termhelper',
      component: termHelper,
    },
    {
      path: '/standalone',
      name: 'standalonePage',
      component: standalonePage,
    },
    {
      path: '/import',
      name: 'importWizardPage',
      component: importWizardPage,
    },
    {
      path: '/export',
      name: 'exportWizardPage',
      component: exportWizardPage,
    },
    {
      path: '/restore',
      name: 'restorePage',
      component: restorePage,
    },
    {
      path: '/test',
      name: 'testPage',
      component: testPage,
    },

  ],
});
