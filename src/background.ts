'use strict';

import path from 'path';

import {
  shell,
  app, protocol, Menu,
  BrowserWindow, webContents, dialog, ipcMain, Event,
} from 'electron';
import {
  createProtocol,
  installVueDevtools,
} from 'vue-cli-plugin-electron-builder/lib';

import { readFileSync, statSync, writeFileSync} from 'fs';

import { CAT_PORT } from './core/params';
import { createCatServer } from './core/catServer';
import { PJT } from './core/pdb';
import {
  DBContentsInfo, StTtMemoDoc, BaseDoc,
  DBFilterQuery, ReadMode,
  VSSyncQue, VSGetDBQue, VSSetDBQue, SVRequireQue, VSRequireQue, PageName,
  GeneralInfo, DataInfo,
  SVSyncQue, ImportQue,
} from './core/pdbIF';
import { ClickOutside } from 'vuetify/lib';

const isDevelopment = process.env.NODE_ENV !== 'production';
export let win: BrowserWindow | null;
export let wcon: webContents | null;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{scheme: 'app', privileges: { secure: true, standard: true } }]);

function createWindow() {
  win = new BrowserWindow({ width: 1000, height: 700, webPreferences: {
    nodeIntegration: true,
    // preload: './preload.js',
  } });
  // win.setAlwaysOnTop(true);
  wcon = win.webContents;
  const menu = Menu.buildFromTemplate(myMenu);
  Menu.setApplicationMenu(menu);

  // 設定を変更できる仕組みを作っておく
  // ファイルは \AppData\\Roaming\\catovis/config.json
  // 将来的にはtomlも使いたい
  let userConfig: any = {applyConfig: false};
  try {
    statSync(app.getPath('userData') + '/config.json');
    const conf = readFileSync(app.getPath('userData') + '/config.json').toString();
    userConfig = JSON.parse(conf);
  } catch (err) {
    console.log('No User Config Data');
  }

  if (userConfig.applyConfig) {
    console.log('config load from config.json');
    PJT.applyUserConfig(userConfig);
  }

  const port = (userConfig.applyConfig && userConfig.port) ? Number(userConfig.port) : CAT_PORT;

  const myCatServer = createCatServer();
  myCatServer.listen(port);
  PJT.setSyncs([SVSyncVue], false);

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
    if (!process.env.IS_TEST) { win.webContents.openDevTools(); }
  } else {
    createProtocol('app');
    win.loadURL('app://./index.html');
  }

  win.on('closed', () => {
    win = null;
  });
}

// MacOS用
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// MacOS用
app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});

// 初期化
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installVueDevtools();
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString());
    }
  }
  createWindow();
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit();
      }
    });
  } else {
    process.on('SIGTERM', () => {
      app.quit();
    });
  }
}

// ##### Electronの初期設定ここまで #####

// Vueの準備ができたら設定を同期する
ipcMain.on('vue-created', (ev: Event): void => {
    ev.sender.send('initialize', { general: PJT.gInfo, MT: PJT.MTsettings, locales: PJT.locales});
});

// VueのImportWizardからファイルを読み込み
ipcMain.on('vs-import-wizard', (ev: Event, impQue: ImportQue): void => {
  switch (impQue.from) {
    case 'JSON':
      importJsonDialog(impQue);
      break;

    case 'TxX':
      if (impQue.into === 'TMs') {
        importTMXDialog(impQue);
      } else if (impQue.into === 'TBs') {
        importTBXDialog(impQue);
      }
      break;

    case 'Excel':
      importXLSXDialog(impQue);
      break;

    case 'CTSV':
      importCTSVDialog(impQue, false);
      break;

    case 'FCTSV':
      importCTSVDialog(impQue, true);
      break;

    case 'FExcel':
      break;

    default:
      break;
  }
  // movePage('/pref');
});

// VueのExportWizardからのリクエストでファイルを書き出す
ipcMain.on('vs-export-wizard', (ev: Event, exQue: any): void => {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openDirectory'],
    title: 'Select Folder to Export',
    defaultPath: '.',
  }, (filePaths) => {
    if (filePaths !== undefined) {
      // TMのエクスポート
      if (exQue.exportTMs.length > 0) {
        if (exQue.merge) {
          const que = { fid: { $in: exQue.exportTMs } };
          PJT.exportTMs(filePaths[0], que, 'merged', exQue);
        } else {
          for (const tmid of exQue.exportTMs) {
            PJT.exportTMs(filePaths[0], { fid: tmid }, PJT.gInfo.tmFilelist[tmid].name, exQue);
          }
        }
      }
      // TBのエクスポート
      if (exQue.exportTBs.length > 0) {
        if (exQue.merge) {
          const que = { fid: { $in: exQue.exportTBs } };
          PJT.exportTBs(filePaths[0], que, 'merged', exQue);
        } else {
          for (const tbid of exQue.exportTBs) {
            PJT.exportTBs(filePaths[0], { fid: tbid }, PJT.gInfo.tbFilelist[tbid].name, exQue);
          }
        }
      }
    }
  });
});

// Vueから情報を反映
ipcMain.on('vs-sync', (ev: Event, que: VSSyncQue): void => {
  if (ev !== null) {
    switch (que.target) {
      // Vueから環境設定を変更する
      case 'pref':
        PJT.setNewPref(que.data);
        break;

      case 'MT':
        PJT.setMTprefs(que.data);
        break;

      // Vueのローカルに入っているバックアップから復旧する
      case 'restore':
        const tmDataset: string[] = que.data;
        const segPairs: StTtMemoDoc[] = [];
        for (const pair of tmDataset) {
          const s: string[] = pair.split('[&]');
          segPairs.push({st: s[0], tt: s[1], memo: ''});
        }
        PJT.loadIntoTMsDB(['CATOVIS Backup'], [segPairs], 'ia');
        if (wcon !== null) {
          const reqQue: SVRequireQue = {
            target: 'message',
            data: 'Restore has done',
          };
          wcon.send('sv-require', reqQue);
        }
        break;

      default:
        break;
    }
  }
});

ipcMain.on('vs-require', (ev: Event, que: VSRequireQue): void => {
  if (ev !== null) {
    switch (que.target) {
      case 'calcSt':
        PJT.calcRatio(que.data.st);
        break;

      case 'newTt':
        PJT.setNewTt(`${que.data.tt}[@]CATOVIS`);
        break;

      case 'dbView':
        shell.openExternal(`http://localhost:${CAT_PORT}/status${que.data}`);
        break;

      case 'onTop':
        if (win !== null) {
          win.setAlwaysOnTop(que.data);
        }
        break;

      case 'term-helper':
        PJT.addTermsHelper();
        break;

      case 'move-page':
        movePage(que.data);
        break;

      case 'add-wholeterm':
        PJT.addWholeTerm();
        break;

      default:
        break;
    }
  }
});

// VueからDBのコンテンツを参照する
ipcMain.on('vs-getFromDB', (ev: Event, que: VSGetDBQue): void => {
  if (ev !== null) {
    switch (que.control) {
      // VueのMiniEdtiorでTMの変更をしようとするとき
      case 'ME':
        // queのkeyIdが-1の場合は、最新のQA結果からのキューであることを示す
        const ekeyId = que.data.keyId === -1 ? PJT.gInfo.tmFilelist[0].nums - 1 : que.data.keyId;
        PJT.toBeAmend(que.target, { fid: que.data.fid, keyId: ekeyId });
        break;

      // Vueからフィルタ表示のリクエストを受けて、ドキュメントの配列を返す
      case 'filter':
        PJT.DBFilterExecuttion(que);
        break;

      default:
        break;
    }
  }
});

ipcMain.on('vs-setToDB', (ev: Event, que: VSSetDBQue): void => {
  switch (que.control) {
    // VueのMiniEdtiorからTMの修正をしようとするとき
    case 'confirmValFromME':
      PJT.reviseTM(que.data);
      break;

    // VueのMiniEdtiorでTMの削除をしようとするとき
    case 'deleteValFromME':
      PJT.deleteTM(que.data);
      break;

    // VueのMiniEditorからTBを追加
    // 追加する単語が一つであっても、配列で渡す
    case 'quickAddTermFromME':
      PJT.addTerms(que.data);
      break;

    case 'confirmTBValFromME':
      PJT.reviseTB(que.data);
      break;

    case 'deleteTBValFromME':
      PJT.deleteTB(que.data);
      break;

    // VuwからTM TBの優先度（fid）を変更
    case 'adjustFid':
      PJT.adjustFid(que.data);
      break;

    // VueからTM TBを外す
    case 'removeDB':
      PJT.removeDB(que.data);
      break;

    default:
      break;
  }
});

// VueのTM TBファイルビューワ用のリクエスト
// ipcMain.on('vs-editorRequest', (ev: Event, edreq: { tmtb: string, idx: number }) => {
//   if (edreq.tmtb === 'tm') {
//     PJT.TMs.find({fid: edreq.idx}, (err: Error, docs: any) => {
//       ev.sender.send('sv-editorContents', docs);
//     });
//   } else if (edreq.tmtb === 'tb') {
//     PJT.TBs.find({fid: edreq.idx}, (err: Error, docs: any) => {
//       ev.sender.send('sv-editorContents', docs);
//     });
//   } else {
//     const noResult: any[] = [];
//     ev.sender.send('sv-editorContents', noResult);
//   }
// });


// メニュー設定
// 具体的な関数は別ファイル（menuFuncs）に記載
const myMenu = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Open',
        click() {
          openPjtZipDialog();
        },
      },
      {
        label: 'Save',
        accelerator: 'Ctrl+S',
        click() {
          savePjtZipDialog(true, false);
        },
      },
      {
        label: 'Save as...',
        submenu: [
          {
            label: 'Save Project (zip) as...',
            click() {
              savePjtZipDialog(false, false);
            },
          },
          {
            label: 'Save & Merge Project (zip)',
            click() {
              savePjtZipDialog(false, true);
            },
          },
        ],
      },
      {
        label: 'Import Files',
        click() {
          movePage('/import');
        },
      },
      {
        label: 'Export Files',
        click() {
          movePage('/export');
        },
      },
      {
        label: 'Backup',
        submenu: [
          {
            label: 'Restore',
            click() {
              movePage('/restore');
              // if (wcon !== null) {
              //   const que: SVRequireQue = {
              //     target: 'restore',
              //     data: null,
              //   };
              //   wcon.send('sv-require', que);
              // }
            },
          },
          {
            label: 'Clear',
            click() {
              if (wcon !== null) {
                const que: SVRequireQue = {
                  target: 'clear backup',
                  data: null,
                };
                wcon.send('sv-require', que);
              }
            },
          },
        ],
      },
      {
        label: 'Close Project',
        click() {
          PJT.closeProject(true, true, true);
        },
      },
    ],
  },
  {
    label: 'Page',
    submenu: [
      {
        label: 'CAT',
        click() {
          movePage('/cat');
        },
      },
      {
        label: 'Term Helper',
        click() {
          movePage('/termhelper');
        },
      },
      // {
      //   label: 'CAT Check',
      //   click() {
      //     movePage('/check');
      //   },
      // },
      {
        label: 'Stand Alone',
        click() {
          movePage('/standalone');
        },
      },
      {
        label: 'MT Settings',
        click() {
          movePage('/prefMT');
        },
      },
      {
        label: 'DB Manager',
        click() {
          movePage('/dbManager');
        },
      },
      {
        label: 'Preference',
        click() {
          movePage('/pref');
        },
      },
      {
        label: 'Filter',
        click() {
          movePage('/filter');
        },
      },
      // {
      //   label: 'TEST',
      //   click() {
      //     movePage('/test');
      //   },
      // },
    ],
  },
  // {
  //   label: 'TM / TB Editor',
  //   submenu: [
  //     {
  //       label: 'TM Editor',
  //       click() {
  //         createEditorWindow();
  //       },
  //     },
  //   ],
  // },
  {
    label: 'Help',
    submenu: [
      {
        label: 'Version',
        click() {
          if (win !== null) {
            dialog.showMessageBox(win, {
              type: 'info',
              title: 'Version',
              message: `CATOVIS Ver. ${PJT.gInfo.ver.ver1}.${PJT.gInfo.ver.ver2}.${PJT.gInfo.ver.ver3}`,
              buttons: ['OK'],
            });
          }
        },
      },
      {
        label: 'How to Use',
        click() {
          shell.openExternal('https://quankaoyang.github.io/catovis-docs/');
        },
      },
      {
        label: 'Advanced Config',
        click() {
          try {
            statSync(app.getPath('userData') + '/config.json');
            shell.openItem(app.getPath('userData') + '/config.json');
          } catch (err) {
            const jsondata: any = {
              applyConfig: false,
              MTInfo: {
                srcLang: 'zh',
                tgtLang: 'ja',
                useGcp: false,
                gcpKey: '',
                useDeepL: false,
                deepLKey: '',
              },
              prefData: {
                upperBound: 125,
                lowerBound: 75,
                ratioLimit: 70,
                realTimeQa: true,
              },
              locales: [ 'ja-jp', 'zh-cn', 'ja-jp-jp', 'en',  'zh-tw', 'zh-hk'],
              port: 8686,
            };
            writeFileSync(app.getPath('userData') + '/config.json', JSON.stringify(jsondata, null, 2));
            shell.openItem(app.getPath('userData') + '/config.json');
          }

        },
      },
      {
        label: 'License',
        click() {
          shell.openExternal('https://quankaoyang.github.io/catovis-docs/LICENSE/');
        },
      },
    ],
  },
  // {
  //   label: 'Dev',
  //   submenu: [
  //     {
  //       label: 'Console',
  //       click() {
  //         if (wcon !== null) {
  //           wcon.toggleDevTools();
  //         }
  //       },
  //     },
  //     {
  //       label: 'Reset Local',
  //       click() {
  //         if (wcon !== null) {
  //           const que: SVRequireQue = {
  //             target: 'clear all',
  //             data: null,
  //           };
  //           wcon.send('sv-require', que);
  //         }
  //       },
  //     },
  //     {
  //       label: 'Reflesh',
  //       click() {
  //         if (wcon !== null) {
  //           const que: SVRequireQue = {
  //             target: 'reflesh',
  //             data: null,
  //           };
  //           wcon.send('sv-require', que);
  //         }
  //       },
  //     },
  //     {
  //       label: 'Show UserFolder',
  //       click() {
  //         console.log(app.getPath('home'));      // -> C:\Users\【ユーザー名】
  //         console.log(app.getPath('appData'));   // -> C:\Users\【ユーザー名】\AppData\Roaming
  //         console.log(app.getPath('userData'));  // -> C:\Users\【ユーザー名】\AppData\Roaming\【パッケージ名】
  //         console.log(app.getPath('temp'));      // -> C:\Users\【ユーザー名】\AppData\Local\Temp
  //         console.log(app.getPath('exe'));       // -> 【実行している.exeのパス】
  //         console.log(app.getPath('module'));    // -> 【実行している.exeのパス】
  //       },
  //     },
  //     {
  //       label: 'Hello, CATOVIS',
  //       click() {
  //         if (wcon !== null) {
  //           const que: SVRequireQue = {
  //             target: 'message',
  //             data: 'Hello, CATOVIS',
  //           };
  //           wcon.send('sv-require', que);
  //         }
  //       },
  //     },
  //   ],
  // },
];

export function SVSyncVue(que: SVSyncQue): void {
  if (wcon !== null) {
    wcon.send('sv-sync', que);
  }
}

export function movePage(moveto: PageName) {
  if (wcon !== null) {
    const que: SVRequireQue = {
      target: 'move',
      moveto,
      data: null,
    };
    wcon.send('sv-require', que);
  }
}

// Open系の関数
// Openは独自形式に対する名称で、ProjectJson | PJT.TMs Database | PJT.TBs Database に使う
export function openPjtZipDialog() {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openFile'],
    title: 'select Project Zip file',
    defaultPath: '.',
    filters: [
      { name: 'Project Zip', extensions: ['pjtzip'] },
    ],
  }, (filePaths) => {
    if (filePaths !== undefined) {
      PJT.openPjtzipAndLoad(filePaths[0]);
    }
  });
}

// Save系の関数
// Saveは独自形式に対する名称で、PJTZIPに使う
export function savePjtZipDialog(overWrite: boolean, merge: boolean): void {
  if (win === null) {
    return;
  }
  if (overWrite && PJT.getPjtPath() !== '') {
    PJT.savePjtZip(PJT.getPjtPath(), false);
  } else {
    dialog.showSaveDialog(win, {
      title: 'Save a Projcet by Zip File',
      defaultPath: '.',
      filters: [
        { name: 'Project Zip', extensions: ['pjtzip'] },
      ],
    }, (filePath: string | undefined) => {
      if (filePath !== undefined) {
        PJT.savePjtZip(filePath, merge);
      }
    });
  }
}

// Import系の関数
// Importは互換形式に対する名称で、TMX | TBX | XLS | CSV/TSV などに使う
// #TODO Promise.allで作り直したい
export function importJsonDialog(que: ImportQue): void | undefined {
  // dbType: string, mode: ReadMode) {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openFile'],
    title: `select ${que.into} Database Json file`,
    defaultPath: '.',
    filters: [
      { name: `${que.into} Database`, extensions: ['json'] },
    ],
  }, (filePaths) => {
    if (filePaths !== undefined) {
      PJT.importJson(filePaths, que);
    }
  });
}

export function importTMXDialog(que: ImportQue): void | undefined {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openFile', 'multiSelections'],
    title: 'select TMX file(s)',
    defaultPath: '.',
    filters: [
      { name: 'TranslationMemory', extensions: ['tmx'] },
    ],
  }, (filePaths) => {
    if (filePaths !== undefined) {
      PJT.importTMX(filePaths, que);
    }
  });
}

// srcLang: string, tgtLang: string, mode: ReadMode): void | undefined {
// #TODO Promise.allで作り直したい
export function importTBXDialog(que: ImportQue): void | undefined {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openFile', 'multiSelections'],
    title: 'select TBX file(s)',
    defaultPath: '.',
    filters: [
      { name: 'Termbase', extensions: ['tbx'] },
    ],
  }, (filePaths) => {
    if (filePaths !== undefined) {
      PJT.importTBX(filePaths, que);
    }
  });
}

// scol: string, tcol: string, mcol: string, header: number,
// mode: ReadMode, into: string): void | undefined {
export function importXLSXDialog(que: ImportQue): void | undefined {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openFile', 'multiSelections'],
    title: 'select Excel file(s)',
    defaultPath: '.',
    filters: [
      { name: 'Excel', extensions: ['xlsx'] },
    ],
  }, (filePaths) => {
    if (filePaths !== undefined) {
      PJT.importXLSX(filePaths, que);
    }
  });
}

// delimiter: string, enclosure: string, header: number,
//   mode: ReadMode, into: string, full: boolean): void | undefined {
export function importCTSVDialog(que: ImportQue, full: boolean): void | undefined {
  if (win === null) {
    return;
  }
  dialog.showOpenDialog(win, {
    properties: ['openFile', 'multiSelections'],
    title: 'select CSV/TSV file(s)',
    defaultPath: '.',
    filters: [
      { name: 'CSV/TSV', extensions: ['csv', 'tsv'] },
    ],
  }, (filePaths) => {
    if (filePaths !== undefined) {
      PJT.importCTSV(filePaths, que, full);
    }
  });
}

