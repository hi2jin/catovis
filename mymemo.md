## 7/29 tslint.json の rulesに以下を追加
 __"no-console": false__
いつか消す。

## いくつかの場所
COPY C:\Users\<USER_NAME>\Documents\Office のカスタム テンプレート
TO   C:\Users\<USER_NAME>\AppData\Roaming\Microsoft\Word\STARTUP

## icoをつくる
[ico変換](https://www.icoconverter.com/)

## CSCを付ける
$env:CSC_LINK ="..\itsol@goldenbridge2002.com.pfx"
$env:CSC_KEY_PASSWORD = pasword

## チュートリアルのマクロのパスワード
@Dianziyangyang

## デバッグ用のコマンド
curl -Method GET "http://localhost:8686/status?dbType=TMs&fid=0"    
curl -Method PATCH "http://localhost:8686/inqure-source" -body "hoge"
curl -Method PATCH "http://localhost:8686/inqure-check" -body "hoge[->]fuga"
curl -Method PATCH "http://localhost:8686/inqure-review" -body "hoge"

