$coreName1 = "CATOVIS_WordTutorial.docm"
$coreName2 = "CATOVIS_WordCore.dotm"
$workingdir = Get-Location

write-host "Are there any changes on CATOVIS VBACore?"
$sw = Read-Host("[y/N]")

if ($sw -eq "y") {

    $file = Get-ChildItem $coreName1 -file
    $wd = New-Object -ComObject "Word.Application"
    $doc = $wd.Documents.open($file.FullName)
    $doc.content.text = ""
    $doc.saveas2("${workingdir}/${coreName2}", 15)
    # For details, see: https://docs.microsoft.com/ja-jp/office/vba/api/word.wdsaveformat
    $doc.close()
    $wd.quit()
    $wd = $null
    [System.GC]::Collect()
    Write-Host "CATOVIS_WordCore.dotm saved at current working direcroy"
}

write-host "Need to execute 'TemplateSet.ps1'?"
$sw = Read-Host("[y/N]")

if ($sw -eq "y") {
    .\TemplateSet.ps1
}


.\advancedConfig.ps1


write-host "Need to execute 'deploy.ps1' now (create zip)?"
$sw = Read-Host("[y/N]")

if ($sw -eq "y") {
    $ver = Read-Host("Input CATOVIS version 'x.x.x' to execute 'deploy.ps1', just type enter to quit:")
    if ($ver -ne "") {
        .\deploy.ps1 $ver
    }
}

