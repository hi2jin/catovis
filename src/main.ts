import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import { ipcRenderer } from 'electron';
import { Event } from 'electron';
import { PrefInfo, GeneralInfo, VSSyncQue, SVRequireQue, SVSyncQue} from './core/pdbIF';
import { Store } from 'vuex';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
  created() {
    console.log('start vue-electron');
    let newBackup = '';
    const ls = localStorage.getItem('_sttt');
    if ( ls === null) {
      newBackup = '';
    } else {
      const currentBackup: string[] = ls.split('{@}').filter((val) => {
        if (val !== '') { return true; }
      });
      if (currentBackup !== undefined) {
        if (currentBackup.length > 3) {
          newBackup = currentBackup.splice(1, 3).join('{@}') + '{@}';
        } else {
          newBackup = currentBackup.join('{@}') + '{@}';
        }
      }
    }
    localStorage.setItem('_sttt', newBackup);
    const vm = this;
    ipcRenderer.send('vue-created');
    ipcRenderer.on('initialize', function(ev: Event, data: { general: GeneralInfo, locales: string[], MT: any } ) {
      vm.$store.commit('InitializeStore', data.general);
      vm.$store.commit('InitializeLocales', data.locales);
      vm.$store.commit('InitializeMT', data.MT);
    });

    ipcRenderer.on('sv-require', function(ev: Event, que: SVRequireQue) {
      switch (que.target) {
        case 'message':
          vm.$store.commit('DisplayMessage', que.data);
          break;
        case 'openME':
          vm.$store.commit('OpenMiniEditor', que.data);
          break;

        case 'move':
          if (que.moveto !== undefined) {
            if (vm.$route.path !== que.moveto) {
              vm.$router.push(que.moveto);
            }
          }
          break;

        case 'clear backup':
          console.log('clear backup');
          localStorage.setItem('_sttt', '{@}');
          break;

        case 'clear all':
          localStorage.clear();
          break;

        case 'reflesh':
          console.log('forth update');
          vm.$forceUpdate();
          break;

        default:
          break;
      }
    });

    ipcRenderer.on('sv-sync', function(ev: Event, que: SVSyncQue) {
      vm.$store.dispatch('SV_Sync', que);
    });
  },
}).$mount('#app');
