write-host "Need to export 'config.json'?"
$sw = Read-Host("[y/N]")

if ($sw -eq "y") {
    $userFoledr = [Environment]::GetFolderPath('ApplicationData')
    cp .\config.json "${userFoledr}\catovis\"
}