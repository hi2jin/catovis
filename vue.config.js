module.exports = {
    publicPath: './',
    pluginOptions: {
        electronBuilder: {
            builderOptions: {
                win: {
                    icon: 'src/assets/catovis.ico',
                },
            }
        }
    }
}