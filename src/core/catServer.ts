import { createServer, Server, IncomingMessage, ServerResponse } from 'http';
import { parse } from 'url';
// import querystring from 'querystring';
import { webContents } from 'electron';

import { win, movePage, wcon } from '../background';
import { PJT } from './pdb';
import { CatInfo, InfoResponse, VSGetDBQue, DBFilterQuery, StTtDoc, StTtMemoDoc, TMDoc, TBDoc } from './pdbIF';

function cnm(data: any) {
    console.log(data);
}

const catovisTag = new RegExp('\\[@λ\\]|___.*?\\[λ@\\]', 'g');

export function createCatServer(): Server {
    const internalServer = createServer();
    internalServer.on('request', (req: IncomingMessage, res: ServerResponse) => {
        const urlParse = parse(req.url as string, true);
        if (req.method === 'GET') {
            switch (urlParse.pathname) {
                // デバッグ用のエンドポイント。ProjectContents.dataをそのままJSONで返す。
                // #TODO デバッグ用最後に消す
                case '/debugstatus':
                    showStatus(res, {dbType: 'all', fid: -1});
                    break;

                case '/status':
                    const dbType = urlParse.query.dbType ? urlParse.query.dbType : 'all';
                    const fid = urlParse.query.fid ? Number(urlParse.query.fid) : -1;
                    if (typeof(dbType) === 'string') {
                        showStatus(res, {dbType, fid});
                    } else {
                        showStatus(res, {dbType: 'all', fid});
                    }

                    break;

                case '/move/cat':
                    if (win !== null) {
                        movePage('/cat');
                        res.end(`moved to page: "/cat"`);
                    } else {
                        res.end('This endpoint is only available in electron...');
                    }
                    break;

                case '/move/termhelper':
                    if (win !== null) {
                        movePage('/termhelper');
                        res.end(`moved to page: "/termhelper"`);
                    } else {
                        res.end('This endpoint is only available in electron...');
                    }
                    break;

                case '/addwholeterm':
                    PJT.addWholeTerm();
                    res.end('Whole Term added');
                    break;

                case '/toggle-ontop':
                    let onTop: boolean = false;
                    if (win !== null) {
                        onTop = !win.isAlwaysOnTop();
                        win.setAlwaysOnTop(onTop);
                        res.end(`Toggled on top mode: ${onTop}`);
                    } else {
                        res.end('This endpoint is only available in electron...');
                    }
                    break;

                // 一括置換用の単語一覧を返すエンドポイント。
                // 原文@@訳文;;という形をとる
                case '/allterms':
                    getTerms(res);
                    break;

                case '/qa':
                    PJT.QaCheck('Man.', '', '');
                    res.end();
                    break;

                default:
                    res.end();
                    break;
            }

        } else if (req.method === 'PUT') {
            switch (urlParse.pathname) {
                case '/addterm':
                    textDispatch(req, res, 'add-term');
                    break;

                case '/qa/st':
                    textDispatch(req, res, 'set-qa-segment', 'st');
                    break;

                case '/qa/tt':
                    textDispatch(req, res, 'set-qa-segment', 'tt');
                    break;

                default:
                    res.end();
                    break;
            }

        } else if (req.method === 'PATCH') {
            switch (urlParse.pathname) {
                case '/inqure-source':
                // 新しい原文を選択し、選択された文に対し、TM／一時TMの検索と用語の注釈をつける
                // Wordなら類似訳文と単語をつなげた文字列を、それ以外ならInfoResponseをJSON.stringfyして返す
                    textDispatch(req, res, 'trans-source');
                    break;

                case '/setnewtrans':
                // 現在の選択に対して訳文をセットする
                    textDispatch(req, res, 'set-target');
                    break;

                case '/to-amend':
                    textDispatch(req, res, 'to-be-amend');
                    break;

                case '/amend-tt':
                    textDispatch(req, res, 'amend-tt');
                    break;

                case '/qfilter':
                // クイックフィルタにテキストを送り込む
                    textDispatch(req, res, 'quick-filter');
                    break;

                case '/inqure-check':
                    // チェック用に 原文[->]訳文 を受け取る
                    textDispatch(req, res, 'check');
                    break;

                case '/inqure-review':
                    textDispatch(req, res, 'review', false);
                    break;

                case '/inqure-review-all':
                    textDispatch(req, res, 'review', true);
                    break;

                case '/inqure-google-mt':
                    textDispatch(req, res, 'google-mt');
                    break;

                default:
                    break;
            }
        }
    });
    return internalServer;
}

// ここからサーバー内の関数
// GET系
// ステータスを表示する
function showStatus(res: ServerResponse, hyperQue: {dbType: string, fid: number}): void {
    res.writeHead(200, { 'Content-Type': 'application/json', 'charset': 'utf-8' });
    const que: any = hyperQue.dbType !== 'TMs' && hyperQue.dbType !== 'TBs' ? {} : {fid: hyperQue.fid};
    const TMoptions: any = hyperQue.dbType === 'all' ? {} : { st: 1, tt: 1, memo: 1, from: 1, _id: 0};
    const TBoptions: any = hyperQue.dbType === 'all' ? {} : { st: 1, tt: 1, memo: 1, _id: 0};
    PJT.TMs.find(que, TMoptions, (tmerr: Error, tmdocs: TMDoc[]) => {
        const tmstats = tmerr === null ? tmdocs : [tmerr];
        PJT.TBs.find(que, TBoptions, (tberr: Error, tbdocs: TBDoc[]) => {
            const tbstats = tberr === null ? tbdocs : [tberr];
            let stats: any;
            if (hyperQue.dbType === 'TMs') {
                stats = {
                    File: hyperQue.fid,
                    Name: PJT.gInfo.tmFilelist[hyperQue.fid].name,
                    Records: tmstats.length,
                    TM: tmstats,
                };
            } else if (hyperQue.dbType === 'TBs') {
                stats = {
                    fid: hyperQue.fid,
                    Name: PJT.gInfo.tbFilelist[hyperQue.fid].name,
                    Records: tbstats.length,
                    TB: tbstats,
                };
            } else {
                stats = {
                    General: PJT.gInfo,
                    Data: PJT.dInfo,
                    TMs: tmstats,
                    TBs: tbstats,
                };
            }
            res.end(JSON.stringify(stats));
        });
    });
}

// 置換用の用語一覧を取得する
function getTerms(res: ServerResponse): void {
    let allterms: string = '';
    const done: string[] = [];
    PJT.TBs.find({ rep: true }).sort( { slen: -1 } ).exec((err: any, docs: any) => {
        if (err !== null) {
            console.log(err);
            res.writeHead(200, { 'Content-Type': 'text/plain', 'charset': 'utf-8' });
            res.end(err);
        } else {
            for (const doc of docs) {
                if (doc.st in done) {
                    continue;
                } else {
                    allterms += `${doc.st}[->]${doc.tt};;`;
                    done.push(doc.st);
                }
            }
            res.writeHead(200, { 'Content-Type': 'text/plain', 'charset': 'utf-8' });
            res.end(allterms);
        }
    });
}

// テキストを使う処理の根元になる関数
// 文字列の読み込みやCATOVISタグの処理を一元処理し、文字列とuser-agentを渡す
function textDispatch(
    req: IncomingMessage, res: ServerResponse, mode: string, additional?: any): void {

    const hostapp: string = req.headers['user-agent'] as string;
    let data: string = '';
    req.on('readable', () => {
        const indata = req.read();
        if (indata !== null) {
            data += indata;
        }
    });
    req.on('end', () => {
        data = data.replace(catovisTag
            , '');
        switch (mode) {
            case 'trans-source':
                inqureSource(res, data, hostapp);
                break;

            case 'set-target':
                setTarget(res, data, hostapp);
                break;

            case 'to-be-amend':
                toBeAmend(res, data, hostapp);
                break;

            case 'amend-tt':
                amendTt(res, data, hostapp);
                break;

            case 'add-term':
                addTerm(res, data, hostapp);
                break;

            case 'set-qa-segment':
                const type = additional === undefined ? 'st' : additional === 'st' ? 'st' : 'tt';
                setQaSegment(res, data, hostapp, type);
                break;

            case 'quick-filter':
                svCallFilter(res, data, hostapp);
                break;

            case 'check':
                inqureCheck(res, data, hostapp);
                break;

            case 'review':
                inqureReview(res, data, hostapp, false);
                break;

            case 'google-mt':
                // inqureGoogleMT(res, data, hostapp);
                inqureMT(res, data, hostapp);
                break;

            default:
                break;
        }
    });
}

// 単文の照会をする
function inqureSource(
    res: ServerResponse, data: string, hostapp: string): void {

    PJT.clearAmend();
    movePage('/cat');
    PJT.calcRatio(data).then((resData: InfoResponse) => {
        // VBAからのリクエストに対しては、VBAが使いやすい形式でレスポンスを返す
        // それ以外はJSONでレスポンスを返す
        if (hostapp === 'VBA') {
            let vbaResponse: string = '';
            for (const sim of resData.data.CAT.similars) {
            vbaResponse += `${sim.st}[->]${sim.tt}[;]`;
            }
            vbaResponse += '[#;]';
            for (const terms of resData.data.CAT.usedTerms) {
                const tts = terms.tts.join('[&]');
                vbaResponse += `${terms.st}[->]${tts}[;]`;
            }
            res.writeHead(200, { 'Content-Type': 'text/plain', 'charset': 'utf-8' });
            res.end(vbaResponse);
        } else {
            res.writeHead(200, { 'Content-Type': 'application/json', 'charset': 'utf-8' });
            res.end(JSON.stringify(resData));
        }
    });
}

// 照会中の原文に対して訳文をセットする
function setTarget(
    res: ServerResponse, data: string, hostapp: string): void {

        PJT.clearAmend();
        PJT.setNewTt(data);
        res.writeHead(200);
        res.end(JSON.stringify(PJT.responder()));
}

function toBeAmend(
    res: ServerResponse, data: string, hostapp: string): void {

    PJT.toBeAmend('TMs', { fid: 0, tt: data });
    res.writeHead(200);
    res.end();
}

function amendTt(
    res: ServerResponse, data: string, hostapp: string): void {

    PJT.amendTt(data);
    res.writeHead(200);
    res.end();
}

function addTerm(
    res: ServerResponse, data: string, hostapp: string): void {

    const termsData: StTtMemoDoc[] = []
    const stttPairs = data.split('[;]')
    for (const stttPair of stttPairs) {
        if (stttPair !== '') {
            const sttt = stttPair.split('[->]');
            termsData.push({
                st: sttt[0],
                tt: sttt[1],
                memo: '',
            });
        }
    }
    PJT.addTerms(termsData);
    res.writeHead(200);
    res.end();
}

// Qa用にセグメントをセットする
function setQaSegment(
    res: ServerResponse, data: string, hostapp: string, type: 'st' | 'tt'): void {

    PJT.dInfo.QA[type] = data;
    res.writeHead(200);
    res.end();
}

// クイックフィルターを呼び出す
function svCallFilter(
    res: ServerResponse, data: string, hostapp: string): void {

    movePage('/filter');
    const fileterQue: DBFilterQuery = {
        fid: [-1], stttor: true,
        st: [data], stexact: false, stor: true,
        tt: [data], ttexact: false, ttor: true,
    };
    const que: VSGetDBQue = {
        target: 'TMTBs',
        control: 'filter',
        data: fileterQue,
    };
    const filterd = PJT.DBFilterExecuttion(que);
    res.writeHead(200);
    res.end();
}

function inqureCheck(
    res: ServerResponse, data: string, hostapp: string): void {

    PJT.clearAmend();
    const sttt = data.split('[->]');
    PJT.calcRatio(sttt[0]).then((resData: InfoResponse) => {
        PJT.calcTransRatio(sttt[1]).then((resData2: InfoResponse) => {
            movePage('/cat');
            // VBAからのリクエストに対しては、VBAが使いやすい形式でレスポンスを返す
            // それ以外はJSONでレスポンスを返す
            if (hostapp === 'VBA') {
                let vbaResponse: string = '';
                for (const sim of resData2.data.CAT.similars) {
                    vbaResponse += `${sim.st}[->]${sim.tt}[;]`;
                }
                vbaResponse += '[#;]';
                for (const terms of resData2.data.CAT.usedTerms) {
                    const tts = terms.tts.join('[&]');
                    vbaResponse += `${terms.st}[->]${tts}[;]`;
                }
                res.writeHead(200, { 'Content-Type': 'text/plain', 'charset': 'utf-8' });
                res.end(vbaResponse);
            } else {
                res.writeHead(200, { 'Content-Type': 'application/json', 'charset': 'utf-8' });
                res.end(JSON.stringify(resData2));
            }
        });
    });
}

function inqureReview(
    res: ServerResponse, data: string, hostapp: string, multi: boolean): void {
    if (multi) {
        console.log('under development');
    } else {
        movePage('/cat');
        PJT.calcReviewRatio(data).then((resData: InfoResponse) => {
        // VBAからのリクエストに対しては、VBAが使いやすい形式でレスポンスを返す
        // それ以外はJSONでレスポンスを返す
            if (hostapp === 'VBA') {
                const score = resData.data.CAT.similars.length > 0 ? resData.data.CAT.similars[0].ratio : 0;
                let vbaResponse: string = `${score}[@;]`;
                for (const sim of resData.data.CAT.similars) {
                    vbaResponse += `${sim.st}[->]${sim.tt}[;]`;
                }
                vbaResponse += '[#;]';
                for (const terms of resData.data.CAT.usedTerms) {
                    const tts = terms.tts.join('[&]');
                    vbaResponse += `${terms.st}[->]${tts}[;]`;
                }
                res.writeHead(200, { 'Content-Type': 'text/plain', 'charset': 'utf-8' });
                res.end(vbaResponse);
            } else {
                res.writeHead(200, { 'Content-Type': 'application/json', 'charset': 'utf-8' });
                res.end(JSON.stringify(resData));
            }
        });
    }
}

// function inqureGoogleMT(
//     res: ServerResponse, data: string, hostapp: string): void {

//     PJT.setGoogleMT(data);
//     res.writeHead(200);
//     res.end();
// }

function inqureMT(
    res: ServerResponse, data: string, hostapp: string): void {

    PJT.setMTs(data);
    res.writeHead(200);
    res.end();
}
