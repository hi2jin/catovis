$TemplateName = "CATOVIS_WordCore.dotm"

$wd = New-Object -ComObject "Word.Application"

$startup = $wd.StartupPath

$wd.Quit()
[System.GC]::Collect()

Write-Host "Please select:"
Write-Host "1. Install CATOVIS Word Core"
Write-Host "2. Uninstall CATOVIS Word Core"
$sw = Read-Host 


if ($sw -eq 1) {
    Copy-Item .\${TemplateName} -Destination $startup -Force
    Write-Host "CATOVIS Word Core has been installed successfully!"
} elseif ($sw -eq 2) {
    Remove-Item ${startup}\${TemplateName}
    Write-Host "CATOVIS Word Core has been uninstalled successfully !"
} else {
    Write-Host "Please select from '1' or '2'"
}

if (test-path "${startup}\SelectAndRequest.dotm") {
    Remove-Item "${startup}\SelectAndRequest.dotm"
}

Read-Host("Please type any key:")