const catovisTag1Left = '<@>'

const catovisTag = new RegExp('\\[@λ\\]|___.*\\[λ@\\]', 'g');

const seg1 = "これは[@λ]単語___Term[λ@]を入れ替えた文です。"
const seg2 = "これは<@~>単語<λ~>Term<~λ><~@>と<@~>単語<λ~>Term<~λ><~@>を入れ替えた文です。"

let rep2 = seg2.replace(catovisTag, '')

console.log(seg1.match(/\[\@λ\]/g))
console.log(seg1.match(catovisTag))
console.log(seg1.replace(catovisTag, ''));
// console.log(rep2);