import Vue from 'vue';
import Vuex from 'vuex';
import { DEF_PREF } from './core/params';
import { PrefInfo, Syncer, SVSyncQue, CatInfo, QaInfo, GeneralInfo } from './core/pdbIF';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    viewMode: '2rp',
    onTop: false,
    windowBG: '',
    catMode: 'T',
    includedTMs: [{ name: 'Inner', nums: 0 }],
    includedTBs: [{ name: 'Inner', nums: 0 }],
    catData: {
      st: '',
      tt: '',
      annotated: '',
      similars: [],
      usedTerms: [],
    },
    QaData: {
      from: '',
      st: '',
      tt: '',
      results: [],
    },
    filterData: {
      TMs: [],
      TBs: [],
    },
    TMSetter: false,
    editingData: {
      displayTM: false,
      displayTB: false,
      fid: 0,
      keyId: 0,
      st: '',
      tt: '',
      memo: '',
      cg: true,
    },
    termHelper: {
      makes: 0,
      textInBracket: {
        st: [],
        tt: [],
      },
      textParallel: {
        st: [],
        tt: [],
      },
    },
    prefData: {
      upperBound: 125,
      lowerBound: 75,
      ratioLimit: 70,
      realTimeQa: true,
    },
    locales: ['ja', 'ja-jp-jp', 'en', 'zh-cn', 'zh-tw', 'zh-hk'],
    MTInfo: {
      srcLang: 'zh',
      tgtLang: 'ja',
      useGcp: false,
      gcpKey: '',
      useDeepL: false,
      deepLKey: '',
    },
    notice: false,
    message: '',
    messageStatus: 'Non',
  },
  getters: {
    TMSetterData(state) {
      if (!state.TMSetter) {
        return '';
      } else if (state.QaData.results.length === 0) {
        return '✓';
      } else {
        return '!';
      }
    },
  },
  mutations: {
    // VueのMainがマウントされた時点で、Mainプロセスから初期設定を読み込む
    InitializeStore(state: any, initialData: GeneralInfo) {
      Vue.set(state, 'includedTMs', initialData.tmFilelist);
      Vue.set(state, 'includedTBs', initialData.tbFilelist);
      Vue.set(state, 'prefData', initialData.pref);
    },
    InitializeLocales(state: any, data: string[]): void {
      Vue.set(state, 'locales', data);
    },
    InitializeMT(state: any, data: any) {
      Vue.set(state, 'MTInfo', data);
    },
    ChangeView(state: any, data: {viewMode: string, onTop: boolean}) {
      state.viewMode = data.viewMode;
      state.onTop = data.onTop;
    },
    SelectCATMode(state: any, data: 'T' | 'C' | 'R') {
      state.catMode = data;
    },
    // ミニエディタを起動する
    // display: booleanとドキュメントのデータを読み込む
    OpenMiniEditor(state: any, data) {
      // Vue.set(state.editingData, 'display', true);
      Vue.set(state.editingData, 'fid', data.fid);
      Vue.set(state.editingData, 'keyId', data.keyId);
      Vue.set(state.editingData, 'st', data.st);
      Vue.set(state.editingData, 'tt', data.tt);
      Vue.set(state.editingData, 'memo', data.memo);
      Vue.set(state.editingData, 'cg', data.cg);
    },
    OpenTMMiniEditor(state: any) {
      Vue.set(state.editingData, 'displayTM', true);
    },
    OpenTBMiniEditor(state: any) {
      Vue.set(state.editingData, 'displayTB', true);
    },
    // ミニエディタを消す
    CloseMiniEditor(state: any) {
      Vue.set(state.editingData, 'displayTM', false);
      Vue.set(state.editingData, 'displayTB', false);
      Vue.set(state.editingData, 'st', '');
      Vue.set(state.editingData, 'tt', '');
      Vue.set(state.editingData, 'memo', '');
      Vue.set(state.editingData, 'cg', false);
    },
    OpenTermHelper(state: any, data: any) {
      state.termHelper.makes++;
      Vue.set(state.termHelper.textInBracket, 'st', data.textInBracket.st);
      Vue.set(state.termHelper.textInBracket, 'tt', data.textInBracket.tt);
      Vue.set(state.termHelper.textParallel, 'st', data.textParallel.st);
      Vue.set(state.termHelper.textParallel, 'tt', data.textParallel.tt);
    },
    // fidを調整してTM/TBの優先順位を変更する
    // Mainプロセスへの反映はdbManageCardから行う
    UpdatePriority(state: any, tmtbFilesData: any) {
      Vue.set(state, 'includedTMs', tmtbFilesData.tms);
      Vue.set(state, 'includedTBs', tmtbFilesData.tbs);
    },
    UpdatePreference(state: any, newPref: any) {
      Vue.set(state, 'prefData', newPref);
    },
    // UpdateTerm(state: any, newTermContents: any) {
    //   const idx = newTermContents.idx;
    //   if (idx === -1) {
    //     state.TBsData.push(newTermContents.newTerm);
    //   } else {
    //     Vue.set(state.TBsData, idx, newTermContents.newTerm);
    //   }
    // },
    // UpdateTermbase(state: any, newTermbase: any) {
    //   const fidx = newTermbase.fidx;
    //   Vue.set(state.TBsData[fidx], 'TB', newTermbase.TB);
    // },
    RemoveTMTBFile(state: any, rmDBQue: any) {
      if (rmDBQue.target === 'TMs') {
        state.includedTMs.splice(rmDBQue.data, 1);
      } else if (rmDBQue.target === 'TBs') {
        state.includedTBs.splice(rmDBQue.data, 1);
      }
    },
    // callTmEditor(state: any, editorData: {idx: number, st: string, tt: string}): void {
    //   state.tmEditor.idx = editorData.idx;
    //   state.tmEditor.st = editorData.st;
    //   state.tmEditor.tt = editorData.tt;
    // },
    update_fid_zero(state: any, data: [number, number]) {
      Vue.set(state.includedTMs[0], 'nums', data[0]);
      Vue.set(state.includedTBs[0], 'nums', data[1]);
    },
    update_CAT(state: any, data: CatInfo) {
      state.TMSetter = false;
      state.catData.st = data.st;
      state.catData.tt = data.tt;
      state.catData.annotated = data.annotated;
      state.catData.similars = data.similars;
      state.catData.usedTerms = data.usedTerms;
    },
    update_TMSetter_and_Backup(state: any, data: {st: string, tt: string, from: string}) {
      state.TMSetter = true;
      const backup: string = `${data.st}[&]${data.tt}[&]${data.from}[;]`;
      if (localStorage.getItem('_sttt') !== null) {
        localStorage.setItem('_sttt', `${localStorage.getItem('_sttt')}${backup}`);
      } else {
        localStorage.setItem('_sttt', `{@}${backup}`);
      }
    },
    // sv_update_TBsData(state: any, newTBs: any) {
    //   Vue.set(state, 'TBsData', newTBs);
    // },
    update_QA(state: any, data: QaInfo) {
      Vue.set(state, 'QaData', data);
    },
    DisplayMessage(state: any, message: string ) {
      state.message = message;
      state.notice = true;
    },
    CloseMessage(state: any) {
      state.notice = false;
    },
    update_includedTMs(state: any, data: any) {
      Vue.set(state, 'includedTMs', data);
    },
    // sv_push_includedTMs(state: any, data: any) {
    //   state.includedTMs.push(data);
    // },
    update_includedTBs(state: any, data: any) {
      Vue.set(state, 'includedTBs', data);
    },
    // sv_push_includedTBs(state: any, data: any) {
    //   state.includedTBs.push(data);
    // },
    update_filtered(state: any, data: any) {
      Vue.set(state.filterData, 'TMs', data[0]);
      Vue.set(state.filterData, 'TBs', data[1]);
    },
    SetMTInfo(state: any, data: any) {
      state.MTInfo.useGcp = data.useGcp;
      state.MTInfo.gcpKey = data.gcpKey;
      state.MTInfo.useDeepL = data.useDeepL;
      state.MTInfo.deepLKey = data.deepLKey;
      state.MTInfo.srcLang = data.srcLang;
      state.MTInfo.tgtLang = data.tgtLang;
    },
  },
  actions: {
    SV_Sync({commit}: any, que: SVSyncQue) {
      console.log(`Got a SVSyncQue: ${que.toSyncs}`);
      for (const syncer of que.toSyncs) {
        switch (syncer.syncTarget) {
          case 'initialize':
            commit('InitializeStore', que.gInfo);
            break;

          case 'CAT':
            commit('update_CAT', que.dInfo.CAT);
            commit('update_fid_zero', [que.gInfo.tmFilelist[0].nums, que.gInfo.tbFilelist[0].nums]);
            if (syncer.syncDetail) {
              commit('SelectCATMode', syncer.syncDetail);
            }
            break;

          case 'Trans-Backup':
            commit('update_CAT', que.dInfo.CAT);
            commit('update_TMSetter_and_Backup', syncer.syncDetail);

          case 'includedTMs':
            commit('update_includedTMs', que.gInfo.tmFilelist);
            break;

          case 'includedTBs':
            commit('update_includedTBs', que.gInfo.tbFilelist);
            break;

          case 'update-TB':
            commit('update_fid_zero', [que.gInfo.tmFilelist[0].nums, que.gInfo.tbFilelist[0].nums]);
            break;

          case 'QA':
            commit('update_QA', que.dInfo.QA);
            break;

          case 'filtered':
            commit('update_filtered', [que.dInfo.filtered.TM, que.dInfo.filtered.TB]);
            break;

          case 'open-TM-ME':
            commit('OpenMiniEditor', que.dInfo.Amending);
            commit('OpenTMMiniEditor');
            break;

          case 'open-TB-ME':
            commit('OpenMiniEditor', que.dInfo.Amending);
            commit('OpenTBMiniEditor');
            break;

          case 'ME-to-close':
            commit('CloseMiniEditor');
            break;

          case 'term-helper':
            commit('OpenTermHelper', syncer.syncDetail);
            break;

          case 'message':
            commit('DisplayMessage', syncer.syncDetail);
            break;

          default:
            break;
        }
      }
    },
  },
});
