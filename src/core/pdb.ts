import { readFileSync, writeFileSync, createWriteStream, write } from 'fs';
import { parseString } from 'xml2js';
import Datastore from 'nedb';
import fetch from 'node-fetch';

import { DEF_PREF } from './params';


import {
    ReadMode,
    PrefInfo,
    StTtDoc, StTtMemoDoc, BaseDoc, AdditionalDoc,
    TMDoc, TBDoc,
    DBContentsInfo,
    Opcode, SimilarInfo, SimilarTargetInfo, CatInfo, UsedTerm,
    QaResultInfo, QaInfo, QAfuncCB,
    VerInfo, GeneralInfo, DataInfo, InfoResponse,
    DBFilterQuery,
    Syncer, SVSyncQue, SVRequireQue, SVSyncCB, VSGetDBQue, VSSetDBQue, ImportQue,
} from './pdbIF';

import QaTerm from '../qafuncs/QaTerm';
import QaNum from '../qafuncs/QaNum';

// import { ipcMain } from 'electron';

function cnm(data: any) {
    console.log(data);
}

export class ProjectInfo {
    public gInfo: GeneralInfo;
    public dInfo: DataInfo;
    public locales: string[];
    public TMs: Datastore;
    public TBs: Datastore;
    public MTsettings: {
        srcLang: string;
        tgtLang: string;
        gcpKey: string;
        useGcp: boolean
        deepLKey: string;
        useDeepL: boolean;
    };
    private d: any;
    private pjtPath: string;
    private syncCBs: SVSyncCB[];
    private QACBs: QAfuncCB[];

    constructor() {
        this.pjtPath = '',
        this.gInfo = {
            ver : {
                // pjtPath: '',
                ver1: 0,
                ver2: 6,
                ver3: 0,
            },
            tmFilelist : [{ name: 'Inner', nums: 0 }],
            tbFilelist : [{ name: 'Inner', nums: 0 }],
            pref : DEF_PREF,
        };

        this.dInfo = {
            CAT: {
                st: '',
                tt: '',
                annotated: '',
                similars: [],
                usedTerms: [],
            },
            QA: {
                from: '',
                st: '',
                tt: '',
                results: [],
            },
            filtered: {
                TM: [],
                TB: [],
            },
            Amending: null,
            message: '',
        };

        this.locales = ['ja', 'ja-jp-jp', 'en', 'zh-cn', 'zh-tw', 'zh-hk'];
        this.MTsettings = {
            srcLang: 'zh',
            tgtLang: 'ja',
            gcpKey: '',
            useGcp: false,
            deepLKey: '',
            useDeepL: false,
        };
        const difflib = require('difflib');
        this.d = new difflib.SequenceMatcher(null, '', '');
        this.TMs = new Datastore();
        this.TBs = new Datastore();
        this.syncCBs = [];
        this.QACBs = [QaTerm, QaNum];
    }

    // 汎用のコールバック
    public messaging(que: SVSyncQue, svsync: SVSyncCB) {
        svsync(que);
    }

    public clearAmend(): void {
        this.dInfo.Amending = null;
    }

    public responder(): InfoResponse {
        const resData = {
            general: this.gInfo,
            data: this.dInfo,
        };
        return resData;
    }

    public setQAs(funcs: QAfuncCB[], replace: boolean) {
        if (replace) {
            this.QACBs.length = 0;
            this.QACBs = [...funcs];
        } else {
            this.QACBs.push(...funcs);
        }
    }

    public setSyncs(funcs: SVSyncCB[], replace: boolean) {
        if (replace) {
            this.syncCBs.length = 0;
            this.syncCBs = [...funcs];
        } else {
            this.syncCBs.push(...funcs);
        }
    }

    public execSyncs(toSyncs: Syncer[]): void {
        const syncQue: SVSyncQue = {
            toSyncs,
            gInfo: this.gInfo,
            dInfo: this.dInfo,
        };
        for (const func of this.syncCBs) {
            func(syncQue);
        }
    }

    public applyUserConfig(data: any): void {
        console.log(data);
        if (data.MTInfo) {
            if (data.MTInfo.srcLang) {
                this.MTsettings.srcLang = data.MTInfo.srcLang;
            }
            if (data.MTInfo.tgtLang) {
                this.MTsettings.tgtLang = data.MTInfo.tgtLang;
            }
            if (data.MTInfo.useGcp) {
                this.MTsettings.useGcp = Boolean(data.MTInfo.useGcp);
            }
            if (data.MTInfo.gcpKey) {
                this.MTsettings.gcpKey = data.MTInfo.gcpKey;
            }
            if (data.MTInfo.useDeepL) {
                this.MTsettings.useDeepL = Boolean(data.MTInfo.useDeepL);
            }
            if (data.MTInfo.deepLKey) {
                this.MTsettings.deepLKey = data.MTInfo.deepLKey;
            }
        }
        if (data.prefData) {
            if (data.prefData.upperBound) {
                this.gInfo.pref.upperBound = Number(data.prefData.upperBound);
            }
            if (data.prefData.lowerBound) {
                this.gInfo.pref.lowerBound = Number(data.prefData.lowerBound);
            }
            if (data.prefData.ratioLimit) {
                this.gInfo.pref.ratioLimit = Number(data.prefData.ratioLimit);
            }
            if (data.prefData.realTimeQa) {
                this.gInfo.pref.realTimeQa = Boolean(data.prefData.realTimeQa);
            }
        }
        if (data.locales) {
            this.locales = data.locales;
        }
    }

    // open時に呼び出して上書き用のパスを保存しておく
    // エクスポートされないようにginfoの外側にある
    public setPjtPath(path: string): void {
        this.pjtPath = path;
    }

    public getPjtPath(): string {
        return this.pjtPath;
    }

    // PJTZIPファイルを開くメソッド
    // PJTZIPファイルのファイルパスを受け取って、gInfoを置き換えていく
    public openPjtzipAndLoad(filePath: string) {
        // 上書き保存用のパスを作っておく
        this.setPjtPath(filePath);
        const jszip = require('jszip');
        const zip = new jszip();
        const read: Buffer = readFileSync(filePath);
        this.TMs.remove({}, { multi: true });
        this.TBs.remove({}, { multi: true });
        let pjtDone = false;
        const syncers: Syncer[] = [];
        syncers.push({
            syncTarget: 'message',
            syncDetail: 'Open Project File',
        });
        PJT.execSyncs(syncers);
        zip.loadAsync(read).then((inzip: any) => {
            inzip.folder('Contents/').forEach((path: string, file: any) => {
                switch (path.substr(0, 2)) {
                    case 'pr':
                        if (!pjtDone) {
                            pjtDone = true;
                            file.async('string').then((pjt: string) => {
                                const pjtJson: any = JSON.parse(pjt);
                                this.gInfo.ver = pjtJson.gInfo.ver;
                                this.gInfo.tmFilelist = pjtJson.gInfo.tmFilelist;
                                this.gInfo.tbFilelist = pjtJson.gInfo.tbFilelist;
                                this.gInfo.pref = pjtJson.gInfo.pref;
                            });
                        }
                        break;

                    case 'tm':
                        file.async('string').then((tm: string) => {
                            const tmJson: any = JSON.parse(tm).data;
                            this.TMs.insert(tmJson);
                        });
                        break;

                    case 'tb':
                        file.async('string').then((tb: string) => {
                            const tbJson: any = JSON.parse(tb).data;
                            this.TBs.insert(tbJson);
                        });
                        break;

                    default:
                        break;
                }
                // this.execSyncs(['initialize']);
            });
        });
    }

    // プロジェクトを閉じる = データをリセットしていく
    public closeProject(general: boolean, data: boolean, others: boolean) {
        if (general) {
            this.pjtPath = '';
            this.gInfo.tmFilelist = [{ name: 'Inner', nums: 0 }];
            this.gInfo.tbFilelist = [{ name: 'Inner', nums: 0 }];
            this.gInfo.pref = DEF_PREF;
        }

        if (data) {
            this.dInfo.CAT = {
                st: '',
                tt: '',
                annotated: '',
                similars: [],
                usedTerms: [],
            };
            this.dInfo.QA = {
                from: '',
                st: '',
                tt: '',
                results: [],
            };
            this.dInfo.filtered.TM.length = 0;
            this.dInfo.filtered.TB.length = 0;
            this.dInfo.message = '';
        }

        if (others) {
            this.TMs.remove({}, { multi: true });
            this.TBs.remove({}, { multi: true });
            this.QACBs = [QaNum, QaTerm];
        // this.syncCBs = [];
        }
    }

    // PJTZIpファイルを保存するメソッド
    public savePjtZip(filePath: string, merge: boolean) {
        // 一度保存した場所は上書き用に使う可能性があるため、保管しておく
        this.setPjtPath(filePath);
        this.savePjtZipInFunc(filePath, merge);
        const syncers: Syncer[] = [];
        syncers.push({
            syncTarget: 'message',
            syncDetail: 'File Saved!',
        });
        this.execSyncs(syncers);
    }

    public setNewPref(pData: PrefInfo) {
        this.gInfo.pref = pData;
    }

    // インポート系のメソッド
    // 複数のファイルを受け入れ可能なため、ファイルパスを配列形式で受け取る
    // JSONファイル
    public importJson(filePaths: string[], que: ImportQue) {
        console.log(filePaths);
        for (const filePath of filePaths) {
            let fid = 0;
            let fileName = 'inner';
            if (que.mode[0] === 'e') {
                fid = que.into === 'TMs' ? this.gInfo.tmFilelist.length : this.gInfo.tbFilelist.length;
                const filePathArray: string[] = filePath.split(/\\/);
                fileName = filePathArray[filePathArray.length - 1];
            }
            this.importJsonInFunc(fid, que.into, que.mode, filePath, fileName);
        }
    }

    // TMファイル srcLang: string, tgtLang: string, mode: ReadMode
    public importTMX(filePaths: string[], que: ImportQue) {
        const TMcontent: StTtMemoDoc[][] = [];
        for (const filePath of filePaths) {
            TMcontent.push(this.tmxReader(filePath, que.srcLang, que.tgtLang));
        }
        this.loadIntoTMsDB(filePaths, TMcontent, que.mode);
    }

    public loadIntoTMsDB(
        filePaths: string[], loadedTM: StTtMemoDoc[][], mode: ReadMode): void {
        const filenums = filePaths.length;
        let counter = this.gInfo.tmFilelist[0].nums;
        let newfid = 0;

        // 読込データが変更禁止（Prohibit）の場合、changeableをfalseにする
        const changeable = mode[1] === 'p' ? false : true;

        // ファイルごとに読込実行
        for (let i = 0; i < filenums; i++) {
            const filePathArray: string[] = filePaths[i].split(/\\/);
            const fileName = filePathArray[filePathArray.length - 1];
            // 外部ファイルとしてDBを読み込む場合の変数設定
            if (mode[0] === 'e') {
                this.gInfo.tmFilelist.push({ name: fileName, nums: loadedTM[i].length });
                newfid = this.gInfo.tmFilelist.length - 1;
                counter = 0;
            } else if (mode[0] === 'i') {
                this.gInfo.tmFilelist[0].nums += loadedTM[i].length;
            }

            // 読込元のファイルごとに、TMドキュメントの配列を作製する
            const newTMs: TMDoc[] = [];
            for (const tm of loadedTM[i]) {
                counter++;
                const newTM: TMDoc =
                    this.createTMDoc(newfid, counter, changeable, tm.st, fileName, tm.tt, tm.memo);
                newTMs.push(newTM);
            }

            // バルクインサートを実行
            // Inner Memoryとして読み込む場合、tmFileList Inner にある数を更新する
            this.TMs.insert(newTMs, (err: Error, docs: TMDoc[]) => {
                if (err !== null) {
                    console.log(err);
                }
                if (mode[0] === 'i') {
                    this.gInfo.tmFilelist[0].nums += docs.length;
                }
            });
        }
        const syncers: Syncer[] = [];
        syncers.push({
            syncTarget: 'includedTMs',
        });
        syncers.push({
            syncTarget: 'message',
            syncDetail: 'TM file(s) has been loaded succesfully',
        });
        this.execSyncs(syncers);
    }

    public importTBX(filePaths: string[], que: ImportQue) {
        const TBcontent: StTtMemoDoc[][] = [];
        for (const filePath of filePaths) {
            TBcontent.push(this.tbxReader(filePath, que.srcLang, que.tgtLang));
        }
        this.loadIntoTBsDB(filePaths, TBcontent, que.mode);
    }

    public fullLoadIntoTMsDB(
        filePaths: string[], loadedTM: TMDoc[][], mode: ReadMode): void {
        const filenums = filePaths.length;
        let counter = this.gInfo.tmFilelist[0].nums;
        let newfid = 0;

        // ファイルごとに読込実行
        for (let i = 0; i < filenums; i++) {
            const filePathArray: string[] = filePaths[i].split(/\\/);
            const fileName = filePathArray[filePathArray.length - 1];
            // 外部ファイルとしてDBを読み込む場合の変数設定
            if (mode[0] === 'e') {
                this.gInfo.tmFilelist.push({ name: fileName, nums: loadedTM[i].length });
                newfid = this.gInfo.tmFilelist.length - 1;
                counter = 0;
            } else if (mode[0] === 'i') {
                this.gInfo.tmFilelist[0].nums += loadedTM[i].length;
            }

            // 読込元のファイルごとに、TMドキュメントの配列を作製する
            const newTMs: TMDoc[] = [];
            for (const tm of loadedTM[i]) {
                counter++;
                const newTM: TMDoc =
                    this.createTMDoc(newfid, counter, tm.cg, tm.st, fileName, tm.tt, tm.memo, tm.ott);
                newTMs.push(newTM);
            }

            // バルクインサートを実行
            // Inner Memoryとして読み込む場合、tmFileList Inner にある数を更新する
            this.TMs.insert(newTMs, (err: Error, docs: TMDoc[]) => {
                if (err !== null) {
                    console.log(err);
                }
                if (mode[0] === 'i') {
                    this.gInfo.tmFilelist[0].nums += docs.length;
                }
            });
        }
        const syncers: Syncer[] = [];
        syncers.push({
            syncTarget: 'includedTMs',
        });
        syncers.push({
            syncTarget: 'message',
            syncDetail: 'TM file(s) has been loaded succesfully',
        });
        this.execSyncs(syncers);
    }

    public loadIntoTBsDB(
        filePaths: string[], loadedTB: StTtMemoDoc[][], mode: ReadMode): void {

        const filenums = filePaths.length;
        let counter = this.gInfo.tbFilelist[0].nums;
        let newfid = 0;

        // 読込データが変更禁止（Prohibit）の場合、changeableをfalseにする
        const changeable = mode[1] === 'p' ? false : true;

        // ファイルごとに読込実行
        for (let i = 0; i < filenums; i++) {
            // 外部ファイルとしてDBを読み込む場合の変数設定
            if (mode[0] === 'e') {
                const filePathArray: string[] = filePaths[i].split(/\\/);
                const fileName = filePathArray[filePathArray.length - 1];
                this.gInfo.tbFilelist.push({ name: fileName, nums: loadedTB[i].length });
                newfid = mode[0] === 'e' ? this.gInfo.tbFilelist.length - 1 : 0;
                counter = 0;
            } else if (mode[0] === 'i') {
                this.gInfo.tbFilelist[0].nums += loadedTB[i].length;
            }

            // 読込元のファイルごとに、TBドキュメントの配列を作製する
            const newTerms: TBDoc[] = [];
            for (const tb of loadedTB[i]) {
                counter++;
                const newTerm: TBDoc =
                    this.createTBDoc(newfid, counter, changeable, tb.st, tb.tt, tb.memo);
                newTerms.push(newTerm);
            }

            // バルクインサートを実行
            // Inner Memoryとして読み込む場合、tbFileList Inner にある数を更新する
            this.TBs.insert(newTerms, (err: Error, docs: TBDoc[]) => {
                if (err !== null) {
                    console.log(err);
                }
                if (mode[0] === 'i') {
                    this.gInfo.tbFilelist[0].nums += docs.length;
                }
            });
            const syncers: Syncer[] = [];
            syncers.push({
                syncTarget: 'includedTBs',
            });
            syncers.push({
                syncTarget: 'message',
                syncDetail: 'TB file(s) has been loaded succesfully',
            });
            this.execSyncs(syncers);
        }
    }

    public fullLoadIntoTBsDB(
        filePaths: string[], loadedTB: TBDoc[][], mode: ReadMode): void {

        const filenums = filePaths.length;
        let counter = this.gInfo.tbFilelist[0].nums;
        let newfid = 0;

        // ファイルごとに読込実行
        for (let i = 0; i < filenums; i++) {
            const filePathArray: string[] = filePaths[i].split(/\\/);
            const fileName = filePathArray[filePathArray.length - 1];
            // 外部ファイルとしてDBを読み込む場合の変数設定
            if (mode[0] === 'e') {
                this.gInfo.tbFilelist.push({ name: fileName, nums: loadedTB[i].length });
                newfid = mode[0] === 'e' ? this.gInfo.tbFilelist.length - 1 : 0;
                counter = 0;
            } else if (mode[0] === 'i') {
                this.gInfo.tbFilelist[0].nums += loadedTB[i].length;
            }

            // 読込元のファイルごとに、TBドキュメントの配列を作製する
            const newTerms: TBDoc[] = [];
            for (const tb of loadedTB[i]) {
                counter++;
                const newTerm: TBDoc =
                    this.createTBDoc(newfid, counter, tb.cg, tb.st, tb.tt, tb.memo, tb.qa, tb.rep);
                newTerms.push(newTerm);
            }

            // バルクインサートを実行
            // Inner Memoryとして読み込む場合、tbFileList Inner にある数を更新する
            this.TBs.insert(newTerms, (err: Error, docs: TBDoc[]) => {
                if (err !== null) {
                    console.log(err);
                }
                if (mode[0] === 'i') {
                    this.gInfo.tbFilelist[0].nums += docs.length;
                }
            });
            const syncers: Syncer[] = [];
            syncers.push({
                syncTarget: 'includedTBs',
            });
            syncers.push({
                syncTarget: 'message',
                syncDetail: 'TB file(s) has been loaded succesfully',
            });
            this.execSyncs(syncers);
        }
    }

    public importXLSX(filePaths: string[], que: ImportQue) {
        this.xlsxReader(filePaths, que.scol, que.tcol, que.mcol, que.header).then((xlsxResult: any) => {
            if (que.into === 'TMs') {
                this.loadIntoTMsDB(filePaths, xlsxResult, que.mode);
            } else if (que.into === 'TBs') {
                this.loadIntoTBsDB(filePaths, xlsxResult, que.mode);
            }
        });
    }

    // into: string, mode: ReadMode,
    // delimiter: string, enclosure: string, header: number, full: boolean)
    public importCTSV(filePaths: string[], que: ImportQue, full: boolean) {

        if (!full) {
            this.ctsvReader(filePaths, que.delimiter, que.enclosure, que.header).then((ctsvResult: StTtMemoDoc[][]) => {
                if (que.into === 'TMs') {
                    this.loadIntoTMsDB(filePaths, ctsvResult, que.mode);
                } else if (que.into === 'TBs') {
                    this.loadIntoTBsDB(filePaths, ctsvResult, que.mode);
                }
            });
        } else {
            if (que.into === 'TMs') {
                this.fullCtsvReaderTM(filePaths, que.delimiter, que.enclosure).then((ctsvResult: any) => {
                    this.fullLoadIntoTMsDB(filePaths, ctsvResult, que.mode);
                });
            } else if (que.into === 'TBs') {
                this.fullCtsvReaderTB(filePaths, que.delimiter, que.enclosure).then((ctsvResult: any) => {
                    this.fullLoadIntoTBsDB(filePaths, ctsvResult, que.mode);
                });
            }
        }
    }

    // エクスポート系のメソッド
    public exportTMs(exDir: string, query: any, name: string, exQue: any) {
        this.TMs.find(query, (err: Error, docs: TMDoc[]) => {
            if (err !== null) {
                console.log(err);
                return;
            }
            switch (exQue.into) {
                case 'JSON':
                    const dbCntents2Write = {
                        type: 'TM',
                        name,
                        nums: docs.length,
                        data: docs,
                    };
                    const JsonWriter = JSON.stringify(dbCntents2Write);
                    writeFileSync(`${exDir}\\TMs_${name}.json`, JsonWriter, { encoding: 'utf-8' });
                    break;
                case 'CTSV':
                    const ecl = exQue.ctsvInfo.enclosure;
                    const dl = `${ecl}${exQue.ctsvInfo.delimiter}${ecl}`;
                    const extension = exQue.ctsvInfo.delimiter === ',' ? 'csv' : 'tsv';
                    const header = ['FID', 'KEYID', 'Source', 'From', 'Target', 'Old Target', 'Memo', 'Revision', 'Editable'];
                    const CTSVWriter = [ecl + header.join(dl) + ecl];
                    for (const rcd of docs) {
                        const line = [
                            rcd.fid,
                            rcd.keyId,
                            rcd.st,
                            rcd.from,
                            rcd.tt,
                            rcd.ott.join(' | '),
                            rcd.memo,
                            rcd.rev,
                            rcd.cg,
                        ];
                        CTSVWriter.push(ecl + line.join(dl) + ecl);
                    }
                    writeFileSync(`${exDir}\\TMs_${name}.${extension}`, CTSVWriter.join('\n'), { encoding: 'utf-8' });
                    break;

                case 'TxX':
                    const ver = `${this.gInfo.ver.ver1}.${this.gInfo.ver.ver2}.${this.gInfo.ver.ver3}`;
                    const sl = exQue.localeInfo.srcLang;
                    const tl = exQue.localeInfo.tgtLang;
                    const attrs = [
                        `creationtool='CATOVIS'`,
                        `creationtoolversion='${ver}'`,
                        `segtype='sentence'`,
                        `adminlang='en'`,
                        `srclang='${sl}'`,
                        `o-tmf='CATOVIS'`,
                        `datatype='unknown'`,
                    ];
                    const attr = attrs.join(' ');
                    const headers = [
                        `<?xml version='1.0' encoding='UTF-8'?>\n`,
                        `<tmx version='1.4'>\n`,
                        `<header ${attr}>\n`,
                        `</header>\n`,
                    ];
                    let tmx = headers.join('') + `<body>\n`;
                    for (const doc of docs) {
                        const uid = doc._id === undefined ? '****************' : doc._id;
                        tmx += `<tu tuid='${uid}'>\n`;
                        tmx += `<tuv xml:lang='${sl}'>\n<seg>${doc.st}</seg>\n</tuv>\n`;
                        tmx += `<tuv xml:lang='${tl}'>\n<seg>${doc.tt}</seg>\n</tuv>\n`;
                        if (doc.memo !== '') {
                            tmx += `<note>${doc.memo}</note>\n`;
                        }
                        tmx += `</tu>\n`;
                    }
                    tmx += '</body>\n</tmx>';
                    writeFileSync(`${exDir}\\TMs_${name}.tmx`, tmx, { encoding: 'utf-8' });
                    break;

                default:
                    break;
            }
        });
    }

    public exportTBs(exDir: string, query: any, name: string, exQue: any) {
        this.TBs.find(query, (err: Error, docs: TBDoc[]) => {
            if (err !== null) {
                console.log(err);
                return;
            }
            switch (exQue.into) {
                case 'JSON':
                    const dbCntents2Write = {
                        type: 'TB',
                        name,
                        nums: docs.length,
                        data: docs,
                    };
                    const JsonWriter = JSON.stringify(dbCntents2Write);
                    writeFileSync(`${exDir}\\TBs_${name}.json`, JsonWriter, { encoding: 'utf-8' });
                    break;
                case 'CTSV':
                    const ecl = exQue.ctsvInfo.enclosure;
                    const dl = `${ecl}${exQue.ctsvInfo.delimiter}${ecl}`;
                    const extension = exQue.ctsvInfo.delimiter === ',' ? 'csv' : 'tsv';
                    const header = ['FID', 'KEYID', 'Source', 'Target', 'Memo', 'QA', 'Replace', 'Editable'];
                    const CTSVWriter = [ecl + header.join(dl) + ecl];
                    for (const rcd of docs) {
                        const line = [rcd.fid, rcd.keyId, rcd.st, rcd.tt, rcd.memo, rcd.qa, rcd.rep, rcd.cg];
                        CTSVWriter.push(ecl + line.join(dl) + ecl);
                    }
                    writeFileSync(`${exDir}\\TBs_${name}.${extension}`, CTSVWriter.join('\n'), { encoding: 'utf-8' });
                    break;

                case 'TxX':
                    const sl = exQue.localeInfo.srcLang;
                    const tl = exQue.localeInfo.tgtLang;
                    let tbx =
                        `<?xml version='1.0' encoding='UTF-8'?>\n<martif xml:lang="en" type="TBX">\n<text>\n<body>\n`;
                    for (const doc of docs) {
                        tbx += `<termEntry>\n`;
                        tbx += `<langSet xml:lang='${sl}'\n>`;
                        tbx += `<tig>\n<term>${doc.st}</term>\n</tig>\n`;
                        tbx += `</langSet>\n`;
                        tbx += `<langSet xml:lang='${tl}'>\n`;
                        tbx += `<tig>\n<term>${doc.tt}</term>\n</tig>\n`;
                        tbx += `</langSet>\n`;
                        tbx += `</termEntry>\n`;
                    }
                    tbx += `</body>\n</text>\n</martif>`;
                    writeFileSync(`${exDir}\\TBs_${name}.tbx`, tbx, { encoding: 'utf-8' });
                    break;

                default:
                    break;
            }
        });
    }

    // 照会された原文に対し、マッチ率を計算する
    public async calcRatio(st: string): Promise<InfoResponse> {
        // 計算のための変数を準備
        return new Promise((resolve, reject) => {
            // 古い訳文データはリセットしておく
            this.closeProject(false, true, false);
            this.dInfo.CAT.st = st;
            const len = st.length;
            const uBound = this.gInfo.pref.upperBound / 100;
            const lBound = this.gInfo.pref.lowerBound / 100;
            this.d.setSeq1(st);
            const prs = [];
            prs.push(this.calcWithTMPromise(len, uBound, lBound));
            prs.push(this.calcWithTBPromise(st));
            Promise.all(prs).then((result) => {
                this.dInfo.CAT.similars = result[0];
                this.dInfo.CAT.annotated = result[1].annotated,
                this.dInfo.CAT.usedTerms = result[1].usedTerms;

                // Electron-Vue使用中はStoreと情報共有
                const syncers: Syncer[] = [];
                syncers.push({
                    syncTarget: 'CAT',
                    syncDetail: 'T',
                });
                this.execSyncs(syncers);

                resolve(this.responder());
            });
            // #TODO catchを書くべき
        });
    }

    public setCatInfo(newInfo: CatInfo): void {
        this.dInfo.CAT = newInfo;
    }

    // 照会された原文に対し、訳文をセットする
    // 訳文のセットは常にInner Momory{fid: 0}に対して行う
    public setNewTt(ttAndFrom: string): void {
        const st = this.dInfo.CAT.st;
        const data = ttAndFrom.split('[@]');
        const tt = data[0];
        const from = data.length > 1 ? data[1] : '';
        const tmDataSet = {st, tt, from};
        this.TMs.findOne({ fid: 0, st }, (err: any, doc: any) => {
            if (err !== null) {
                console.log(err);
            // 同一の原文がTMsになかった場合、新規登録し、tmFileList Inner にある数を更新するを更新する
            } else if (doc === null) {
                this.dInfo.CAT.tt = tt;
                const newTM: TMDoc = this.createTMDoc(0, this.gInfo.tmFilelist[0].nums, true, st, from, tt);
                this.TMs.insert(newTM);
                this.gInfo.tmFilelist[0].nums++;
                const syncers: Syncer[] = [];
                syncers.push({
                    syncTarget: 'Trans-Backup',
                    syncDetail: tmDataSet,
                });
                this.execSyncs(syncers);

                // realTimeQaがtrueであれば、QaCheckも同時に行う
                if (this.gInfo.pref.realTimeQa) {
                    this.QaCheck('NEW', st, tt);
                }

            // 同一の原文がTMsにあった場合、DBのttを更新する
            // 元のttは所定の数までottに格納しておく
            // TODO ottの格納数を変数化
            } else {
                if (doc.tt !== tt) {
                    this.dInfo.CAT.tt = tt;
                    const otts: string[] = doc.ott.splice(0, 0, doc.tt);
                    if (otts.length > 3) {
                        otts.length = 3;
                    }
                    this.TMs.update({ _id: doc._id }, {$set: { tt, ott: otts }});
                    if (this.gInfo.pref.realTimeQa) {
                        this.QaCheck('REV', st, tt);
                    }
                    const syncers: Syncer[] = [];
                    syncers.push({
                        syncTarget: 'Trans-Backup',
                        syncDetail: tmDataSet,
                    });
                    this.execSyncs(syncers);
                }
            }
        });
    }

    public toBeAmend(dbType: string, DBque: any): void {
        if (dbType === 'TMs') {
            this.TMs.findOne(DBque, (err: Error, doc: TMDoc) => {
                if (err !== null) {
                    this.clearAmend();
                    console.log(err);
                    return;
                } else if (doc === null) {
                    this.clearAmend();
                    return;
                } else {
                    this.dInfo.Amending = doc;
                    const syncers: Syncer[] = [];
                    syncers.push({
                        syncTarget: 'open-TM-ME',
                    });
                    this.execSyncs(syncers);
                }
            });
        } else if (dbType === 'TBs') {
            this.TBs.findOne(DBque, (err: Error, doc: TMDoc) => {
                if (err !== null) {
                    this.clearAmend();
                    console.log(err);
                    return;
                } else if (doc === null) {
                    this.clearAmend();
                    return;
                } else {
                    this.dInfo.Amending = doc;
                    const syncers: Syncer[] = [];
                    syncers.push({
                        syncTarget: 'open-TB-ME',
                    });
                    this.execSyncs(syncers);
                }
            });
        }
    }

    public amendTt(newText: string): void {
        if (this.dInfo.Amending !== null && this.dInfo.Amending._id !== undefined) {
            const reviseQue = {
                st: this.dInfo.Amending.st,
                tt: newText,
                memo: this.dInfo.Amending.memo,
            };
            this.reviseTM(reviseQue);
            const syncers: Syncer[] = [];
            syncers.push({
                syncTarget: 'ME-to-close',
            });
            this.execSyncs(syncers);
        }
    }

    public calcTransRatio(tt: string): Promise<InfoResponse> {
        return new Promise((resolve, reject) => {
            this.dInfo.CAT.tt = tt;
            this.d.setSeq1(tt);
            if (this.dInfo.CAT.similars.length === 0) {
                const syncers: Syncer[] = [];
                syncers.push({
                    syncTarget: 'CAT',
                    syncDetail: 'C',
                });
                this.execSyncs(syncers);
                resolve(this.responder());
            } else {
                for (const simInfo of this.dInfo.CAT.similars) {
                    this.d.setSeq2(simInfo.tt);
                    const simTargetResult: SimilarTargetInfo = {
                        ttInTM: simInfo.tt,
                        tgtRatio: Math.floor(this.d.ratio() * 100),
                        tgtOpcode: this.d.getOpcodes(),
                    };
                    simInfo.toCheck = simTargetResult;
                }

                // Electron-Vue使用中はStoreと情報共有
                const syncers: Syncer[] = [];
                syncers.push({
                    syncTarget: 'CAT',
                    syncDetail: 'C',
                });
                this.execSyncs(syncers);

                resolve(this.responder());
            }
        });
    }

    // レビュー用の訳文vs訳文の計算
    public async calcReviewRatio(tt: string): Promise<InfoResponse> {
        // 計算のための変数を準備
        return new Promise((resolve, reject) => {
            // 古い訳文データはリセットしておく
            this.closeProject(false, true, false);
            this.dInfo.CAT.tt = tt;
            const len = tt.length;
            const uBound = this.gInfo.pref.upperBound / 100;
            const lBound = this.gInfo.pref.lowerBound / 100;
            this.d.setSeq1(tt);
            const tmcheck = this.calcReviewWithTMPromise(len, uBound, lBound);
            tmcheck.then((tmresult) => {
                if (tmresult.length > 0) {
                    this.dInfo.CAT.similars = tmresult;
                    const st = tmresult[0].st;
                    const tbcheck = this.calcReviewWithTBPromise(st);
                    tbcheck.then((tbresult) => {
                        this.dInfo.CAT.annotated = tbresult.annotated,
                        this.dInfo.CAT.usedTerms = tbresult.usedTerms;
                        const qacheck = this.QaCheckPromise('Review', st, tt);
                        qacheck.then((errNums) => {
                            this.dInfo.CAT.similars[0].ratio -= errNums;
                            // Electron-Vue使用中はStoreと情報共有
                            const syncers: Syncer[] = [];
                            syncers.push({
                                syncTarget: 'CAT',
                                syncDetail: 'R',
                            });
                            syncers.push({
                                syncTarget: 'QA',
                            });
                            this.execSyncs(syncers);
                            resolve(this.responder());
                        });
                    });
                } else {
                    const syncers: Syncer[] = [];
                    syncers.push({
                        syncTarget: 'CAT',
                        syncDetail: 'R',
                    });
                    syncers.push({
                        syncTarget: 'QA',
                    });
                    this.execSyncs(syncers);
                    resolve(this.responder());
                }
            });
        });
    }

    // ミニエディタからTMの修正をする
    // もしくはWordからの修正を受け付ける
    public reviseTM(revTMQue: any): void {
        const fid = this.dInfo.Amending === null ? 0 : this.dInfo.Amending.fid;
        const keyId = this.dInfo.Amending === null ? this.gInfo.tmFilelist[0].nums - 1 : this.dInfo.Amending.keyId;
        const emptyOption = {};
        this.TMs.findOne({ $and: [{ fid }, { keyId }] }, (err: Error, doc: TMDoc) => {
            if (err !== null) {
                console.log(err);
                return;
            } else if (doc === null) {
                return;
            } else {
                const otts: string[] = doc.ott.splice(0, 0, doc.tt);
                if (otts.length > 3) {
                    otts.length = 3;
                }
                const updateQue = {
                    $set: {
                        st: revTMQue.st,
                        tt: revTMQue.tt,
                        slen: revTMQue.st.length,
                        tlen: revTMQue.tt.length,
                        ott: otts,
                        memo: revTMQue.memo,
                    },
                };
                this.TMs.update({_id: doc._id}, updateQue, emptyOption, (err2: Error, numOfDocs: number) => {
                    if (err !== null) {
                        console.log(err2);
                    } else {
                        console.log(`TM update: ${numOfDocs}`);
                    }
                });
            }
        });
    }

    // ミニエディタからTMを削除する
    public deleteTM(delTMQue: any): void {
        const updateEmptyQue = {
            $set: {
                st: '',
                tt: '',
                slen: -1,
                tlen: -1,
                memo: '',
            },
        };
        const emptyOption = {};
        if (this.dInfo.Amending !== null) {
            this.TMs.update({ $and: [{ fid: this.dInfo.Amending.fid }, { keyId: this.dInfo.Amending.keyId }] },
                updateEmptyQue, emptyOption, (err: Error) => {
                    if (err !== null) {
                        console.log(err);
                    }
                });
        }
    }

    // 用語を追加するメソッド
    // 用語の追加はは常にInner Momory{fid: 0}に対して行う
    public addTerms(termData: StTtMemoDoc[]) {
        const prs: Array<Promise<boolean>> = [];
        const termToSet: TBDoc[] = [];
        const self = this;
        let tempKeyId = self.gInfo.tbFilelist[0].nums;
        for (const newTerm of termData) {
            prs.push(self.termCheckPromise(newTerm, tempKeyId));
        }
        Promise.all(prs).then(function(result: boolean[]) {
            for (let i = 0; i <= result.length; i++) {
                if (result[i]) {
                    termToSet.push(
                        PJT.createTBDoc(0, tempKeyId, true, termData[i].st, termData[i].tt, termData[i].memo),
                        );
                    tempKeyId++;
                    if (self.dInfo.CAT.st.indexOf(termData[i].st) !== -1) {
                        PJT.dInfo.CAT.usedTerms.push({
                            fid: 0,
                            keyId: tempKeyId,
                            st: termData[i].st,
                            tts: [termData[i].tt],
                            memo: '',
                            times: 1,
                        });
                    }
                }
            }
            self.TBs.insert(termToSet, (err: Error, doc: any) => {
                if (err !== null) {
                    console.log(err);
                    return;
                }
                self.gInfo.tbFilelist[0].nums = tempKeyId;
                const syncers: Syncer[] = [];
                syncers.push({
                    syncTarget: 'update-TB',
                });
                syncers.push({
                    syncTarget: 'CAT',
                });
                syncers.push({
                    syncTarget: 'message',
                    syncDetail: `${doc.length} term(s) has been added`,
                });
                self.execSyncs(syncers);
            });
        });
    }

    public termCheckPromise(termData: StTtMemoDoc, keyId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.TBs.findOne({ fid: 0, st: termData.st, tt: termData.tt}, (err: Error, doc: TBDoc) => {
                if (err !== null) {
                    reject(err);
                } else if (doc === null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    public addTermsHelper(): void {
        const inBracket = new RegExp(`(「.*?」|『.*?』|【.*?】|《.*?》|＜.*?＞|“.*?”|\".*?\"|\'.*?\')`, 'g');
        // const brackets = new RegExp(`[「」『』【】《》＜＞“”\"\']`, 'g')
        const stInBraket = this.dInfo.CAT.st.match(inBracket);
        const ttInBraket = this.dInfo.CAT.tt.match(inBracket);
        // if (stInBraket !== null) {
        //     stInBraket = stInBraket.map(val => {
        //         return val.replace(brackets, '');
        //     });
        // } else {
        //     stInBraket = []
        // }
        // if (ttInBraket !== null) {
        //     ttInBraket = ttInBraket.map(val => {
        //         return val.replace(brackets, '');
        //     });
        // } else {
        //     ttInBraket = []
        // }
        const textInBracket = {
            st: stInBraket === null ? [] : stInBraket,
            tt: ttInBraket === null ? [] : stInBraket,
        };

        const delimiter = new RegExp('(、|，|,| and | or |および|及び|または|又は|あるいは|或いは|もしくは|及|以及|或|或者)', 'g');
        const stParallel = this.dInfo.CAT.st.replace(delimiter, '$1|').split('|');
        const ttParallel = this.dInfo.CAT.tt.replace(delimiter, '$1|').split('|');
        const textParallel = {
            st: stParallel === null ? [] : stParallel,
            tt: ttParallel === null ? [] : ttParallel,
        };
        const syncers: Syncer[] = [];
        syncers.push({
            syncTarget: 'term-helper',
            syncDetail: {textInBracket, textParallel},
        });
        this.execSyncs(syncers);
    }

    public addWholeTerm(): void {
        if (this.dInfo.CAT.st === '' || this.dInfo.CAT.tt === '') {
            return;
        }
        const wholeTerm: StTtMemoDoc[] = [{
            st: this.dInfo.CAT.st,
            tt: this.dInfo.CAT.tt,
            memo: '',
        }];
        this.addTerms(wholeTerm);
    }

    // ミニエディタからTBの修正をする
    public reviseTB(revTBQue: any): void {
        const emptyOption = {};
        this.TBs.findOne({ $and: [{ fid: revTBQue.fid }, { keyId: revTBQue.keyId }] }, (err: Error, doc: TBDoc) => {
            if (err !== null) {
                console.log(err);
                return;
            } else if (doc === null) {
                return;
            } else {
                const updateQue = {
                    $set: {
                        st: revTBQue.st,
                        tt: revTBQue.tt,
                        slen: revTBQue.st.length,
                        tlen: revTBQue.tt.length,
                        memo: revTBQue.memo,
                    },
                };
                this.TBs.update({_id: doc._id}, updateQue, emptyOption, (err2: Error, numOfDocs: number) => {
                    if (err !== null) {
                        console.log(err2);
                    } else {
                        console.log(`TB Update: ${numOfDocs}`);
                    }
                });
            }
        });
    }

    // ミニエディタからTBを削除する
    public deleteTB(delTBQue: any): void {
        const updateEmptyQue = {
            $set: {
                st: '',
                tt: '',
                slen: -1,
                tlen: -1,
                memo: '',
            },
        };
        const emptyOption = {};
        this.TBs.update({ $and: [{ fid: delTBQue.fid }, { keyId: delTBQue.keyId }] },
            updateEmptyQue, emptyOption, (err: Error) => {
                if (err !== null) {
                    console.log(err);
                }
            });
    }

    // DBの順番を変える
    public adjustFid(newFidQue: any): void {
        this.gInfo.tmFilelist = newFidQue.this.TMs;
        this.gInfo.tbFilelist = newFidQue.this.TBs;
        if (newFidQue.tmsMoving.length > 0) {
            for (const newPair of newFidQue.tmsMoving) {
                this.TMs.update({fid : newPair[0]}, -1, {multi: true});
                this.TMs.update({fid : newPair[1]}, newPair[0], {multi: true});
                this.TMs.update({fid : -1}, newPair[1], {multi: true});
            }
        }
        if (newFidQue.tbsMoving.length > 0) {
            for (const newPair of newFidQue.tbsMoving) {
                this.TBs.update({fid : newPair[0]}, -1, {multi: true});
                this.TBs.update({fid : newPair[1]}, newPair[0], {multi: true});
                this.TBs.update({fid : -1}, newPair[1], {multi: true});
            }
        }
    }

    public removeDB(rmDBQue: any): void {
        if (rmDBQue.type === 'TM') {
            this.TMs.remove({fid : rmDBQue.fid}, {multi : true});
        } else if (rmDBQue.type === 'TB') {
            this.TBs.remove({fid : rmDBQue.fid}, {multi : true});
        }
    }

    // Qaに関する関数
    public QaCheck(from: string, st: string, tt: string): void {
        const QaExecuting = [];
        const results: QaResultInfo[] = [];

        // QaInfoにデータをセットしていく
        // 直接stとttを関数からセットされた場合はここでセットする
        this.dInfo.QA.from = from;
        if (st === '') {
            st = this.dInfo.QA.st;
        } else {
            this.dInfo.QA.st = st;
        }

        if (tt === '') {
            tt = this.dInfo.QA.tt;
        } else {
            this.dInfo.QA.tt = tt;
        }

        for (const qaFunc of this.QACBs) {
            QaExecuting.push(qaFunc(this.dInfo));
        }

        Promise.all(QaExecuting).then((allQaResults) => {
            for (const each of allQaResults) {
                results.push(...each);
            }
            this.dInfo.QA.results = results;
            const syncers: Syncer[] = [];
            syncers.push({
                syncTarget: 'QA',
            });
            this.execSyncs(syncers);
        });
    }

    public QaCheckPromise(from: string, st: string, tt: string): Promise<number> {
        return new Promise((resolve, reject) => {
            const QaExecuting = [];
            const results: QaResultInfo[] = [];

            // QaInfoにデータをセットしていく
            // 直接stとttを関数からセットされた場合はここでセットする
            this.dInfo.QA.from = from;
            if (st === '') {
                st = this.dInfo.QA.st;
            } else {
                this.dInfo.QA.st = st;
            }
            if (tt === '') {
                tt = this.dInfo.QA.tt;
            } else {
                this.dInfo.QA.tt = tt;
            }

            for (const qaFunc of this.QACBs) {
                QaExecuting.push(qaFunc(this.dInfo));
            }

            Promise.all(QaExecuting).then((allQaResults) => {
                for (const each of allQaResults) {
                    results.push(...each);
                }
                this.dInfo.QA.results = results;
                resolve(results.length);
            });
        });
    }


    // 【---DBフィルタに関するメソッド---】
    public DBFilterExecuttion(que: VSGetDBQue): Promise<any> {
        return new Promise((resolve, reject) => {
            const filterQue = this.DBFilterQueCreation(que.data);
            const prs = [];
            switch (que.target) {
                case 'TMs':
                prs.push(this.DBFilterPromise(this.TMs, filterQue));
                prs.push(this.DBZeroFilterPromise());
                break;

            case 'TBs':
                prs.push(this.DBZeroFilterPromise());
                prs.push(this.DBFilterPromise(this.TBs, filterQue));
                break;

            case 'TMTBs':
                prs.push(this.DBFilterPromise(this.TMs, filterQue));
                prs.push(this.DBFilterPromise(this.TBs, filterQue));
                break;

            default:
                break;
            }
            Promise.all(prs).then((tmtbdocs) => {
                this.dInfo.filtered.TM = tmtbdocs[0];
                this.dInfo.filtered.TB = tmtbdocs[1];
                const syncers: Syncer[] = [];
                syncers.push({
                    syncTarget: 'filtered',
                });
                this.execSyncs(syncers);
                resolve(this.responder());
            });
        });
    }

    public setMTprefs(data: {
            srcLang: string, tgtLang: string,
            useGcp: boolean, gcpKey: string,
            useDeepL: boolean, deepLKey: string,
        }) {

        this.MTsettings.srcLang = data.srcLang;
        this.MTsettings.tgtLang = data.tgtLang;
        this.MTsettings.useGcp = data.useGcp;
        this.MTsettings.gcpKey = data.gcpKey;
        this.MTsettings.useDeepL = data.useDeepL;
        this.MTsettings.deepLKey = data.deepLKey;
    }

    public setMTs(text: string): void {
        const prs = [];
        const syncers: Syncer[] = [];
        if (!this.MTsettings.useGcp && !this.MTsettings.useDeepL) {
            syncers.push({
                syncTarget: 'message',
                syncDetail: 'No MT is set now!',
            });
            this.execSyncs(syncers);
        }
        if (this.MTsettings.useGcp) {
            if (this.MTsettings.gcpKey === '') {
                syncers.push({
                    syncTarget: 'message',
                    syncDetail: 'Please set API Key(Google)!',
                });
                this.execSyncs(syncers);
            } else {
                prs.push(this.getGoogleMTPromise(text));
            }
        }

        if (this.MTsettings.useDeepL) {
            if (this.MTsettings.deepLKey === '') {
                syncers.push({
                    syncTarget: 'message',
                    syncDetail: 'Please set API Key(DeepL)!',
                });
                this.execSyncs(syncers);
            } else {
                prs.push(this.getDeepLMTPromise(text));
            }
        }

        Promise.all(prs).then((results: SimilarInfo[]) => {
            this.dInfo.CAT.similars.push(...results);
            syncers.push({
                syncTarget: 'CAT',
                syncDetail: 'T',
            });
            this.execSyncs(syncers);
        }).catch(() => {
            syncers.push({
                syncTarget: 'message',
                syncDetail: 'Get MT failed',
            });
            this.execSyncs(syncers);
        });
    }

    // savePjtZipから呼び出す子関数
    private savePjtZipInFunc(filePath: string, merge: boolean) {
        const jszip = require('jszip');
        const zip = new jszip();
        const zipFolder = zip.folder('Contents');
        const gInfo = {
            gInfo: this.gInfo,
        };
        zipFolder.file('project.json', JSON.stringify(gInfo));
        const prs = [];
        if (merge) {
            prs.push(
                this.dbContentsWritePromise(this.TMs, 'TM', 'Inner', true, 0),
                this.dbContentsWritePromise(this.TBs, 'TB', 'Inner', true, 0),
            );
            Promise.all(prs).then((tmtbcontents) => {
                zipFolder.file('tm_0_Inner.json', JSON.stringify(tmtbcontents[0]));
                zipFolder.file('tb_0_Inner.json', JSON.stringify(tmtbcontents[1]));
                zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
                    .pipe(createWriteStream(filePath))
                    .on('finish', function() {
                        console.log('pjtzip written.');
                    });
            });
        } else {
            const tmNums = this.gInfo.tmFilelist.length;
            const tbNums = this.gInfo.tbFilelist.length;
            for (let i = 0; i < tmNums; i++) {
                prs.push(
                    this.dbContentsWritePromise(this.TMs, 'TM', this.gInfo.tmFilelist[i].name, false, i),
                );
            }
            for (let j = 0; j < tbNums; j++) {
                prs.push(
                    this.dbContentsWritePromise(this.TBs, 'TB', this.gInfo.tbFilelist[j].name, false, j),
                );
            }
            Promise.all(prs).then((tmtbcontents) => {
                for (let i = 0; i < tmNums; i++) {
                    zipFolder.file(`tm_${i}_${this.gInfo.tmFilelist[i].name}.json`,
                        JSON.stringify(tmtbcontents[i]));
                }
                for (let j = 0; j < tbNums; j++) {
                    zipFolder.file(`tb_${j}_${this.gInfo.tbFilelist[j].name}.json`,
                        JSON.stringify(tmtbcontents[tmNums + j]));
                }
                zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
                    .pipe(createWriteStream(filePath))
                    .on('finish', function() {
                        console.log('pjtzip written.');
                    });
            });
        }
    }

    // savePjtZipInFuncから呼び出す子関数
    // DBを書き出すためのPromiseを返す
    private async dbContentsWritePromise(
        DB: Datastore, fileType: string, fileName: string, merge: boolean, modFid: number) {
        return new Promise((resolve, reject) => {
            const modify = merge ? {} : { fid: modFid };
            DB.find(modify, (err: Error, docs: any[]) => {
                if (err !== null) {
                    reject(err);
                } else {
                    const dbCntents2Write = {
                        type: fileType,
                        name: fileName,
                        nums: docs.length,
                        data: docs,
                    };
                    resolve(dbCntents2Write);
                }
            });
        });
    }

    private importJsonInFunc(
        nfid: number, dbType: string, mode: ReadMode,
        filePath: string, fileName: string): void {

        const db = dbType === 'TMs' ? this.TMs : this.TBs;
        db.remove({}, { multi: true });
        const read: string = readFileSync(filePath, 'utf-8');
        const dbRecords = JSON.parse(read).data;

        for (const dbRecord of dbRecords) {
            if (mode[0] === 'i') {
                dbRecord.fid = 0;
            } else {
                dbRecord.fid = nfid;
            }

            if (mode[1] === 'a') {
                dbRecord.cg = true;
            } else {
                dbRecord.cg = false;
            }
        }

        db.insert(dbRecords, (err: Error, docs: any[]) => {
            if (mode[0] === 'i') {
                if (dbType === 'TMs') {
                    this.gInfo.tmFilelist[0].nums += dbRecords.length;
                } else if (dbType === 'TBs') {
                    this.gInfo.tbFilelist[0].nums += dbRecords.length;
                }
            } else if (mode[0] === 'e') {
                const ndb: DBContentsInfo = {
                    name: fileName,
                    nums: dbRecords.length,
                };

                if (dbType === 'TMs') {
                    this.gInfo.tmFilelist.push(ndb);
                } else if (dbType === 'TBs') {
                    this.gInfo.tbFilelist.push(ndb);
                }
            }
        });

        const syncers: Syncer[] = [];
        syncers.push({
            syncTarget: `included${dbType}`,
        });
        this.execSyncs(syncers);
    }

    private tmxReader(filePath: string, srcLang: string, tgtLang: string): StTtMemoDoc[] {
        const tmcontent: StTtMemoDoc[] = [];
        const read: string = readFileSync(filePath, 'utf-8');
        // TMX内のフォーマットタグにまだ対応していないため、削除しておく
        const tmxTagRemove = new RegExp('<(b|e)pt.*<\/(b|e)pt>', 'g');
        // TradosやMemesource等、ソフトによってロケールは大文字・小文字が混在しているため、正規表現で判定する
        const srcReg = new RegExp(srcLang, 'i');
        const tgtReg = new RegExp(tgtLang, 'i')
        parseString(read, (err: Error, result: any) => {
            if (err) {
                console.log(err);
            } else {
                if (result.tmx.header[0].$.srclang !== srcLang) {
                    return '';
                }
                const transUnits = result.tmx.body[0].tu;
                for (const transUnit of transUnits) {
                    const tmpair: StTtMemoDoc = { st: '', tt: '', memo: '' };
                    for (const tuv of transUnit.tuv) {
                        const lang = tuv.$['xml:lang'];
                        // if (lang === srcLang) {
                        if (srcReg.test(lang)) {
                            tmpair.st = String(tuv.seg[0]).replace(tmxTagRemove, '');
                        // } else if (lang === tgtLang) {
                        } else if (tgtReg.test(lang)) {
                            tmpair.tt = String(tuv.seg[0]).replace(tmxTagRemove, '');
                        } else {
                            continue;
                        }
                    }
                    if (tmpair.st !== '' && tmpair.tt !== '') {
                        // 実際のTMには同じセグメントペアが多く含まれているため、完全に同一なものを削除しておく
                        let dupli = false;
                        for (const inTM of tmcontent) {
                            if (inTM.st === tmpair.st && inTM.tt === tmpair.tt) {
                                dupli = true;
                                break;
                            }
                        }
                        if (!dupli) {
                            tmcontent.push(tmpair);
                        }
                    }
                }
            }
        });
        return tmcontent;
    }

    private tbxReader(filePath: string, srcLang: string, tgtLang: string): StTtMemoDoc[] {
        const tbcontent: StTtMemoDoc[] = [];
        const read: string = readFileSync(filePath, 'utf-8');
        parseString(read, (err: Error, result: any) => {
            if (err) {
                console.log(err);
            } else {
                const termEntries = result.martif.text[0].body[0].termEntry;
                for (const termEntry of termEntries) {
                    const tbpair: StTtMemoDoc = { st: '', tt: '', memo: '' };
                    for (const langSet of termEntry.langSet) {
                        const lang = langSet.$['xml:lang'];
                        if (lang === srcLang) {
                            tbpair.st = langSet.tig[0].term[0];
                        } else if (lang === tgtLang) {
                            tbpair.tt = langSet.tig[0].term[0];
                        } else {
                            continue;
                        }
                    }
                    if (tbpair.st !== '' && tbpair.tt !== '') {
                        tbcontent.push(tbpair);
                    }
                }
            }
        });
        return tbcontent;
    }

    private async xlsxReader(xlsxFiles: string[], scol: string, tcol: string, mcol: string, header: number) {
        return new Promise((resolve, reject) => {
            // const loaded: StTtMemoDoc[][] = [];
            // 環境設定から原文列・訳文列・メモ列を設定
            const sCol = new RegExp('^' + scol + '[0-9]+$');
            const tCol = new RegExp('^' + tcol + '[0-9]+$');
            const mCol = new RegExp('^' + mcol + '[0-9]+$');
            // zipの中身を読み込む。sheet1のみ
            const prs: any[] = [];
            for (const xlsxPath of xlsxFiles) {
                prs.push(this.readExcelPromise(xlsxPath, sCol, tCol, mCol, header));
            }
            Promise.all(prs).then((allResult) => {
                resolve(allResult);
            });
        });
    }

    private async readExcelPromise(xlsxPath: string, sCol: RegExp, tCol: RegExp, mCol: RegExp, header: number) {
        return new Promise((resolve, reject) => {
            const xValues: StTtMemoDoc[] = [];
            const jszip = require('jszip');
            const zip = new jszip();
            const read: Buffer = readFileSync(xlsxPath);
            zip.loadAsync(read).then((inzip: any) => {
                inzip.folder('xl/').file('sharedStrings.xml').async('string').then((stxml: string) => {
                    // sharedStringから文字列を読み込み、allstringsに格納する
                    const allstrings: string[] = [];
                    // const sharedStrings = (new DOMParser()).parseFromString(stxml, 'application/xml');
                    parseString(stxml, (err: Error, sharedStrings: any) => {
                        if (err) {
                            reject(err);
                        } else {
                            // const siNodes = sharedStrings.sst[0].si;
                            const sstNode = sharedStrings.sst;
                            const siNodes = sstNode.si;
                            for (const siNode of siNodes) {
                                let textInSiNode = '';
                                if (siNode.r === undefined) {
                                    if (siNode.t !== undefined) {
                                        textInSiNode += siNode.t;
                                    }
                                } else {
                                    for (const rNode of siNode.r) {
                                        if (rNode.rPh !== undefined) {
                                            continue;
                                        } else {
                                            textInSiNode += rNode.t;
                                        }
                                    }
                                }
                                allstrings.push(textInSiNode);
                            }
                            inzip.folder('xl/worksheets/').file('sheet1.xml').async('string').then((shxml: string) => {
                                parseString(shxml, (err2: Error, ws1: any) => {
                                    if (err2) {
                                        reject(err2);
                                    } else {
                                        // rowのノードコレクション
                                        const sheetData = ws1.worksheet.sheetData;
                                        const rowNodes = sheetData[0].row;
                                        // rowごとに処理を実行
                                        // ヘッダー分row配列を飛ばす
                                        // 原文列・訳文列・メモ列のみ、SharedStringの対応番号を読み込む
                                        let svalue = '';
                                        let tvalue = '';
                                        let mvalue = '';
                                        for (const rowNode of rowNodes.slice(header)) {
                                            if (svalue !== '' && tvalue !== '') {
                                                const xValue: StTtMemoDoc = {
                                                    st: svalue,
                                                    tt: tvalue,
                                                    memo: mvalue,
                                                };
                                                xValues.push(xValue);
                                            }
                                            svalue = '';
                                            tvalue = '';
                                            mvalue = '';
                                            const cellNodes = rowNode.c;
                                            let gotS = false;
                                            let gotT = false;
                                            let gotM = false;
                                            for (const cellNode of cellNodes) {
                                                const cellRange = cellNode.$.r;
                                                if (!gotS) {
                                                    if (sCol.test(cellRange) && cellNode.$.t === 's') {
                                                        svalue = allstrings[Number(cellNode.v[0])];
                                                        gotS = true;
                                                    }
                                                } else if (!gotT) {
                                                    if (tCol.test(cellRange) && cellNode.$.t === 's') {
                                                        tvalue = allstrings[Number(cellNode.v[0])];
                                                        gotT = true;
                                                    }
                                                } else if (!gotM) {
                                                    if (mCol.test(cellRange) && cellNode.$.t === 's') {
                                                        mvalue = allstrings[Number(cellNode.v[0])];
                                                        gotM = true;
                                                    }
                                                } else {
                                                    break;
                                                }
                                            }
                                        }
                                        resolve(xValues);
                                    }
                                });
                            });
                        }
                        // SharedStringのparseString のCBここまで
                    });
                });
            });
        });
    }

    private async ctsvReader(
        filePaths: string[], delimiter: string, enclosure: string, header: number)
    : Promise<StTtMemoDoc[][]> {
        return new Promise((resolve, reject) => {
            const prs: any[] = [];
            for (const filePath of filePaths) {
                prs.push(this.ctsvReaderPromise(filePath, delimiter, enclosure, header));
            }
            Promise.all(prs).then((allResult) => {
                resolve(allResult);
            });
        });
    }

    private async ctsvReaderPromise(
        filePath: string, delimiter: string, enclosure: string, header: number)
        : Promise<StTtMemoDoc[]> {
        return new Promise((resolve, reject) => {
            const read: string = readFileSync(filePath, 'utf-8');
            const separator: string = enclosure === '' ? delimiter : `${enclosure}${delimiter}${enclosure}`;
            const result: StTtMemoDoc[] = [];
            // 読み込みのための二次元配列をつくる
            let texts = read.replace(/\r\n/g, '\n');
            if (enclosure !== '') {
                texts = texts.substr(1, texts.length - 2);
            }
            const lines = texts.split('\n');
            for (const line of lines.slice(header)) {
                if (line !== '') {
                    const inline = line.split(separator);
                    const inlineSeg = {
                        st: inline[0],
                        tt: inline.length >= 2 ? inline[1] : '',
                        memo: inline.length >= 3 ? inline[2] : '',
                    };
                    result.push(inlineSeg);
                }
            }
            // 読み取った値を返す
            resolve(result);
        });
    }

    private async fullCtsvReaderTM(filePaths: string[], delimiter: string, enclosure: string): Promise<TMDoc[][]> {
        return new Promise((resolve, reject) => {
            const prs: any[] = [];
            for (const filePath of filePaths) {
                prs.push(this.fullCtsvReaderTMPromise(filePath, delimiter, enclosure));
            }
            Promise.all(prs).then((allResult) => {
                resolve(allResult);
            }).catch(() => {
                console.log('some error has occured');
            });
        });
    }

    private async fullCtsvReaderTMPromise(filePath: string, delimiter: string, enclosure: string): Promise<TMDoc[]> {
        return new Promise((resolve, reject) => {
            const read: string = readFileSync(filePath, 'utf-8');
            const separator: string = enclosure === '' ? delimiter : `${enclosure}${delimiter}${enclosure}`;
            const result: TMDoc[] = [];
            // 読み込みのための二次元配列をつくる
            const texts = read.replace(/\r\n/g, '\n');
            const lines = texts.split('\n');
            cnm(lines[0].split(separator).join(''));
            if (lines[0].split(separator).join('') !== `${enclosure}FIDKEYIDSourceFromTargetOld TargetMemoRevisionEditable${enclosure}`) {
                reject();
            } else {
                for (const line of lines.slice(1)) {
                    if (line !== '') {
                        const inline = line.split(separator);
                        const inlineDoc: TMDoc = {
                            fid: Number(inline[0].replace(enclosure, '')),
                            keyId: Number(inline[1]),
                            st: inline[2],
                            from: inline[3],
                            tt: inline[4],
                            ott: inline[5].split(' | '),
                            memo: inline[6],
                            rev: Number(inline[7]),
                            cg: inline[8].replace(enclosure, '') === 'true',
                            slen: inline[2].length,
                            tlen: inline[4].length,
                        };
                        result.push(inlineDoc);
                    }
                }
                // 読み取った値を返す
                resolve(result);
            }
        });
    }

    private async fullCtsvReaderTB(filePaths: string[], delimiter: string, enclosure: string): Promise<TBDoc[][]> {
        return new Promise((resolve, reject) => {
            const prs: any[] = [];
            for (const filePath of filePaths) {
                prs.push(this.fullCtsvReaderTBPromise(filePath, delimiter, enclosure));
            }
            Promise.all(prs).then((allResult) => {
                resolve(allResult);
            });
        });
    }

    private async fullCtsvReaderTBPromise(filePath: string, delimiter: string, enclosure: string): Promise<TBDoc[]> {
        return new Promise((resolve, reject) => {
            const read: string = readFileSync(filePath, 'utf-8');
            const separator: string = enclosure === '' ? delimiter : `${enclosure}${delimiter}${enclosure}`;
            const result: TBDoc[] = [];
            // 読み込みのための二次元配列をつくる
            let texts = read.replace(/\r\n/g, '\n');
            if (enclosure !== '') {
                texts = texts.substr(1, texts.length - 2);
            }
            const lines = texts.split('\n');
            if (lines[0].split(separator).join('') !== 'FIDKEYIDSourceTargetMemoQAReplaceEditable') {
                reject();
            } else {
                for (const line of lines.slice(1)) {
                    if (line !== '') {
                        const inline = line.split(separator);
                        const inlineDoc: TBDoc = {
                            fid: Number(inline[0]),
                            keyId: Number(inline[1]),
                            st: inline[2],
                            tt: inline[3],
                            memo: inline[4],
                            qa: inline[5] === 'true',
                            rep: inline[6] === 'true',
                            cg: inline[7] === 'true',
                            slen: inline[2].length,
                            tlen: inline[3].length,
                        };
                        result.push(inlineDoc);
                    }
                }
                // 読み取った値を返す
                resolve(result);
            }
        });
    }

    // TMドキュメント用のインターフェースに沿ってオブジェクトを作る
    private createTMDoc(
        fid: number, keyId: number, cg: boolean,
        st: string, from: string,
        tt?: string | undefined, memo?: string | undefined, oldtts?: string[] | undefined): TMDoc {
        const ott: string[] = oldtts === undefined ? [] : oldtts;
        const tm: TMDoc = {
            fid,
            keyId,
            st,
            from,
            tt: tt === undefined ? '' : tt,
            memo: memo === undefined ? '' : memo,
            ott,
            rev: 0,
            slen: st.length,
            tlen: tt === undefined ? 0 : tt.length,
            cg,
        };
        return tm;
    }

    // TBドキュメント用のインターフェースに沿ってオブジェクトを作る
    private createTBDoc(
        fid: number, keyId: number, cg: boolean,
        st: string, tt: string, memo?: string | undefined, qa?: boolean | undefined, rep?: boolean | undefined): TBDoc {
        const tb: TBDoc = {
            fid,
            keyId,
            st,
            tt,
            memo: memo === undefined ? '' : memo,
            slen: st.length,
            tlen: tt === undefined ? 0 : tt.length,
            qa: qa === undefined ? true : qa,
            rep: rep === undefined ? true : rep,
            cg,
        };
        return tb;
    }

    private calcWithTMPromise(len: number, uBound: number, lBound: number): Promise<SimilarInfo[]> {
        return new Promise((resolve, reject) => {
            const que = { slen: { $gte: len * lBound, $lte: len * uBound } };
            // TMから取得するのは、文字数がある程度近いものだけ
            this.TMs.find(que, (err: Error, docs: TMDoc[]) => {
                if (err !== null) {
                    reject(err);
                } else {
                    let similars: SimilarInfo[] = [];
                    const ratioLimit = this.gInfo.pref.ratioLimit;
                    for (const tm of docs) {
                        const seg = tm.st;
                        this.d.setSeq2(seg);
                        const r = Math.floor(this.d.ratio() * 100);
                        // 一致率が設定した下限より高い場合、類似文として登録する
                        // ミニエディタで使えるよう、fidとkeyIdも取得しておく
                        if (r > ratioLimit) {
                            const simResult: SimilarInfo = {
                                st: tm.st,
                                tt: tm.tt,
                                memo: tm.memo,
                                from: tm.from,
                                fid: tm.fid,
                                keyId: tm.keyId,
                                ratio: r,
                                opcode: this.d.getOpcodes(),
                            };
                            similars.push(simResult);
                        }
                    }
                    // 一致率が高いものから降順に並び替え
                    similars = similars.sort((a, b) => {
                        if (a.ratio < b.ratio) { return 1; }
                        if (a.ratio > b.ratio) { return -1; }
                        return 0;
                    });
                    resolve(similars);
                }
            });
        });
    }

    private calcReviewWithTMPromise(len: number, uBound: number, lBound: number): Promise<SimilarInfo[]> {
        return new Promise((resolve, reject) => {
            const que = { tlen: { $gte: len * lBound, $lte: len * uBound } };
            // TMから取得するのは、文字数がある程度近いものだけ
            this.TMs.find(que, (err: Error, docs: TMDoc[]) => {
                if (err !== null) {
                    reject(err);
                } else {
                    let similars: SimilarInfo[] = [];
                    const ratioLimit = this.gInfo.pref.ratioLimit;
                    for (const tm of docs) {
                        const seg = tm.tt;
                        this.d.setSeq2(seg);
                        const r = Math.floor(this.d.ratio() * 100);
                        // 一致率が設定した下限より高い場合、類似文として登録する
                        // ミニエディタで使えるよう、fidとkeyIdも取得しておく
                        if (r > ratioLimit) {
                            const simResult: SimilarInfo = {
                                st: tm.st,
                                tt: tm.tt,
                                memo: tm.memo,
                                from: tm.from,
                                fid: tm.fid,
                                keyId: tm.keyId,
                                ratio: r,
                                opcode: [],
                                toCheck: {
                                    ttInTM: tm.tt,
                                    tgtRatio: r,
                                    tgtOpcode: this.d.getOpcodes(),
                                },
                            };
                            similars.push(simResult);
                        }
                    }
                    // 一致率が高いものから降順に並び替え
                    similars = similars.sort((a, b) => {
                        if (a.ratio < b.ratio) { return 1; }
                        if (a.ratio > b.ratio) { return -1; }
                        return 0;
                    });
                    resolve(similars);
                }
            });
        });
    }

    private calcWithTBPromise(st: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.TBs.find({slen: { $lte: st.length }}, (err: Error, docs: TBDoc[]) => {
                if (err !== null) {
                    reject(err);
                } else {
                    // annotatedはHTMLで評価するのでサニタイズしておく
                    let annotated = st.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    const tempTerms: string[] = [];
                    const usedTerms: UsedTerm[] = [];
                    let counter = 0;
                    for (const term of docs) {
                        if (term.st === '') {
                            continue;
                        }
                        const sterm = new RegExp(term.st, 'g');
                        const matches = st.match(sterm);
                        if (matches !== null) {
                            if (term.st in tempTerms) {
                                const idx = tempTerms.indexOf(term.st);
                                usedTerms[idx].tts.push(term.tt);
                            } else {
                                annotated = annotated.replace(
                                    sterm, '[_#' + String(counter) + '_]');
                                const usedTerm: UsedTerm = {
                                    fid: term.fid,
                                    keyId: term.keyId,
                                    st: term.st,
                                    tts: [term.tt],
                                    memo: term.memo,
                                    times: matches.length,
                                };
                                usedTerms.push(usedTerm);
                                counter++;
                            }
                        }
                    }
                    resolve({annotated, usedTerms});
                }
            });
        });
    }

    private calcReviewWithTBPromise(st: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.TBs.find({slen: { $lte: st.length }}, (err: Error, docs: TBDoc[]) => {
                if (err !== null) {
                    reject(err);
                } else {
                    let annotated = st.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    const tempTerms: string[] = [];
                    const usedTerms: UsedTerm[] = [];
                    let counter = 0;
                    for (const term of docs) {
                        const sterm = new RegExp(term.st, 'g');
                        const matches = st.match(sterm);
                        if (matches !== null) {
                            if (term.st in tempTerms) {
                                const idx = tempTerms.indexOf(term.st);
                                usedTerms[idx].tts.push(term.tt);
                            } else {
                                annotated = annotated.replace(
                                    sterm, '[_#' + String(counter) + '_]');
                                const usedTerm: UsedTerm = {
                                    fid: term.fid,
                                    keyId: term.keyId,
                                    st: term.st,
                                    tts: [term.tt],
                                    memo: term.memo,
                                    times: matches.length,
                                };
                                usedTerms.push(usedTerm);
                                counter++;
                            }
                        }
                    }
                    resolve({annotated, usedTerms});
                }
            });
        });
    }

    // DBにフィルタをかけるためのキューオブジェクトを生成する関数
    // st/tt は配列で受け取る。
    private DBFilterQueCreation( fq: DBFilterQuery): any {
        // st tt ともに空の場合はnullを返す
        // fidのみで全文表示できるようにするかは検討中
        if (fq.st[0] === '' && fq.tt[0] === '') {
            return null;
        }
        // fidに関するキューを作る。-1を受け取った場合は、すべてのfidから検索する
        const fidq: null | any = fq.fid[0] === -1 ? null : { fid: { $in: fq.fid } };

        // stに関するキューを作る。空の場合はundefinedとなる
        let stq;
        if (fq.st.length !== 0) {
            // 曖昧一致の場合は正規表現を使って生成していく
            if (!fq.stexact) {
                const sts = [];
                for (const ast of fq.st) {
                    if (ast !== '') {
                        const strgx = new RegExp('.*' + ast + '.*');
                        sts.push({ st: { $regex: strgx } });
                    }
                }
                if (fq.stor) {
                    stq = { $or: sts };
                } else {
                    stq = { $and: sts };
                }
                // 完全一致の場合は文字列を使用する
            } else {
                if (fq.st.length === 1) {
                    stq = { st: fq.st[0] };
                } else {
                    stq = { st: { $in: fq.st } };
                }
            }
        }

        // ttに関するキューを作る。空の場合はundefinedとなる
        let ttq;
        if (fq.tt.length !== 0) {
            // 曖昧一致の場合は正規表現を使って生成していく
            if (!fq.ttexact) {
                const tts = [];
                for (const att of fq.tt) {
                    if (att !== '') {
                        const ttrgx = new RegExp('.*' + att + '.*');
                        tts.push({ tt: { $regex: ttrgx } });
                    }
                }
                if (fq.ttor) {
                    ttq = { $or: tts };
                } else {
                    ttq = { $and: tts };
                }
                // 完全一致の場合は文字列を使用する
            } else {
                if (fq.tt.length === 1) {
                    ttq = { tt: fq.tt[0] };
                } else {
                    ttq = { tt: { $in: fq.tt } };
                }
            }
        }

        // st / tt を合わせたキューをつくる
        // いずれかがundefinedの場合は、一方のみになる
        let stttQue;
        if (stq === undefined) {
            stttQue = ttq;
        } else if (ttq === undefined) {
            stttQue = stq;
        } else {
            stttQue = fq.stttor ? { $or: [stq, ttq] } : { $and: [stq, ttq] };
        }
        const filterQue = fidq === null ? stttQue : { $and: [fidq, stttQue] };

        return filterQue;
    }

    // DBフィルタ実行用のプロミスを返すメソッド
    // 検索対象であった場合
    private DBFilterPromise(db: Datastore, fq: any): Promise<BaseDoc[]> {
        return new Promise((resolve, reject) => {
          db.find(fq).sort({fid: 1}).exec((err: Error, docs: any[]) => {
            if (err !== null) {
              reject(err);
            } else {
              resolve(docs);
            }
          });
        });
      }

    // DBフィルタ実行用のプロミスを返すメソッド
    // 検索対象でなかった場合も空の配列をresolveする
    private DBZeroFilterPromise(): Promise<[]> {
        return new Promise((resolve, reject) => {
            resolve([]);
            });
        }

    // Google Cloud Translation用のAPI Keyを保存する
    // public setGCPtransAPI(data: { key: string, srcLang: string, tgtLang: string }): void {
    //     this.gcptrans.key = data.key;
    //     this.gcptrans.source = data.source;
    //     this.gcptrans.target = data.target;
    // }

    // Google Cloud Translationを呼び出して、Similarにセットする
    private getGoogleMTPromise(text: string): Promise<SimilarInfo> {
        return new Promise((resolve, reject) => {
            const que = {
                q: text,
                source: this.MTsettings.srcLang,
                target: this.MTsettings.tgtLang,
            };
            const endpoint = `https://translation.googleapis.com/language/translate/v2?key=${this.MTsettings.gcpKey}`;
            fetch(endpoint, {method: 'POST', body: JSON.stringify(que) }).then((res) => {
                res.json().then((tranlated) => {
                    const mt: SimilarInfo = {
                        st: text,
                        tt: tranlated.data.translations[0].translatedText,
                        ratio: -1,
                        keyId: -1,
                        fid: -1,
                        memo: '',
                        from: `Google Machine Translation`,
                        opcode: [],
                    };
                    resolve(mt);
                }).catch((err) => {
                    reject(err);
                    });
            }).catch((fetcherr) => {
                console.log(`Fetch Error: ${fetcherr}`);
            });
        });
    }

    private getDeepLMTPromise(text: string): Promise<SimilarInfo> {
        return new Promise((resolve, reject) => {
            const que = {
                q: text,
                auth_key: this.MTsettings.deepLKey,
                source_lang: this.MTsettings.srcLang.toUpperCase(),
                target_lang: this.MTsettings.tgtLang.toUpperCase(),
            };
            const endpoint = `https://api.deepl.com/v2/translate?auth_key=${this.MTsettings.deepLKey}`;
            fetch(endpoint, {method: 'POST', body: JSON.stringify(que) }).then((res) => {
                res.json().then((tranlated) => {
                    const mt: SimilarInfo = {
                        st: text,
                        tt: tranlated.translations[0].text,
                        ratio: -1,
                        keyId: -1,
                        fid: -1,
                        memo: '',
                        from: `DeepL Machine Translation`,
                        opcode: [],
                    };
                    resolve(mt);
                }).catch((err) => {
                    reject(err);
                    });
            }).catch((fetcherr) => {
                console.log(`Fetch Error: ${fetcherr}`);
            });
        });
    }


// ProjectInformation クラス　ここまで
}

export const PJT = new ProjectInfo();
