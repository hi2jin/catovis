import { PrefInfo } from './pdbIF';

// Jupyterの8888、Vueの8080と競合するため 8686　へ変更。Linuxで使用できるかは要確認
export const CAT_PORT: number = 8686;

export const DEF_PREF: PrefInfo = {
    upperBound: 125,
    lowerBound: 75,
    ratioLimit: 70,
    realTimeQa: true,
};

export const LOCALES: string[] = ['ja', 'ja-jp-jp', 'en', 'zh-cn', 'zh-tw', 'zh-hk'];

// TMを読み込むときの原文
// #TODO 設定UIから変更できるようにする
export const SOURCELANG: string = 'ja-jp-jp';

// TMを読み込むときの訳文
// #TODO 設定UIから変更できるようにする
export const TARGETLANG: string = 'zh-cn';

// 文字数の比較に使う。この文字数比率以上離れていた場合、マッチをとらない
export const BOUNDARY: number = 0.25;

// この数字以下のマッチ率は類似とみなさない。
export const RATIO_LIMIT: number = 70;
