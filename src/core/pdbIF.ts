// ##### Core ProjectInfo 用のインターフェース #####

// pjtzipに保存しておくべき情報
// 変更回数は少ない
export interface GeneralInfo {
    ver: VerInfo;
    tmFilelist: DBContentsInfo[];
    tbFilelist: DBContentsInfo[];
    pref: PrefInfo;
}

// バージョン情報
export interface VerInfo {
    ver1: number;
    ver2: number;
    ver3: number;
}

// データベースの状態
// ファイル名と数量を管理
export interface DBContentsInfo {
    name: string;
    nums: number;
}

// 環境設定を管理
export interface PrefInfo {
    upperBound: number;
    lowerBound: number;
    ratioLimit: number;
    realTimeQa: boolean;
}

// 保存時に書き出さない情報
// 頻繁に書き換えを行う
export interface DataInfo {
    CAT: CatInfo;
    QA: QaInfo;
    filtered: {
        TM: BaseDoc[],
        TB: BaseDoc[],
    };
    Amending: TMDoc | null;
    message: string;
}

// 特定の原文セグメントに対するCAT情報
// 原文、単語置き換え原文、一致情報、使用されている単語
// チェック用に訳文と訳文の一致情報も追加
export interface CatInfo {
    st: string;
    tt: string;
    annotated: string;
    similars: SimilarInfo[];
    usedTerms: UsedTerm[];
}

// 特定のTMとの一致情報
export interface SimilarInfo {
    st: string;
    tt: string;
    memo: string;
    from: string;
    fid: number;
    keyId: number;
    ratio: number;
    opcode: Opcode[];
    toCheck?: SimilarTargetInfo;
}

export interface SimilarTargetInfo {
    ttInTM: string;
    tgtRatio: number;
    tgtOpcode: Opcode[];
}

// オペコードのタイプ。類似分の表示に使用
export type Opcode = [string, number, number, number, number];

// CAT情報内の使用している用語
// 一つの原文に対して、複数の訳文用語があることがあるため、ttは配列
export interface UsedTerm {
    fid: number;
    keyId: number;
    st: string;
    tts: string[];
    memo: string;
    times: number;
}

// Qaの結果をまとめたインターフェース
// 原文、訳文、結果一覧を持つ
export interface QaInfo {
    from: string;
    st: string;
    tt: string;
    results: QaResultInfo[];
}

// Qaエラーの結果を保存するインターフェース
// errtypeには用語違い、数字違いなどの情報が入る
// sInfo tInfoにはエラーの具体的な内容が入る
export interface QaResultInfo {
    errtype: string;
    sInfo: string;
    tInfo: string;
}

// PJTの状態を返すときの型
export interface InfoResponse {
    general: GeneralInfo;
    data: DataInfo;
}

// ##### データベースに格納するドキュメントの型
export interface StTtDoc {
    st: string;
    tt: string;
}

export interface StTtMemoDoc extends StTtDoc {
    memo: string;
}

export interface BaseDoc extends StTtMemoDoc {
    fid: number;
    keyId: number;
    slen: number;
    tlen: number;
    _id?: string;
}

export interface AdditionalDoc extends BaseDoc {
    cg: boolean;
    // from: string,
}

// TMドキュメント用のインターフェース
export interface TMDoc extends AdditionalDoc {
    from: string;
    ott: string[];
    rev: number;
}

// TBドキュメント用のインターフェース
export interface TBDoc extends AdditionalDoc {
    qa: boolean;
    rep: boolean;
}

// DBから必要なものを取ってくるためのクエリー
// この情報を元に、DBへのクエリーを作製する
// !!正規表現の部分は検証必要
export interface DBFilterQuery {
    fid: number[];
    stttor: boolean;
    st: string[];
    stexact: boolean;
    stor: boolean;
    tt: string[];
    ttexact: boolean;
    ttor: boolean;
}

// VueとMainプロセスのやり取りに使用するデータの型
// Main -> Vuex
export interface Syncer {
    syncTarget: string;
    syncDetail?: any;
}

export interface SVSyncQue {
    gInfo: GeneralInfo;
    dInfo: DataInfo;
    toSyncs: Syncer[];
    moveto?: PageName;
}

// PJT内に置いておく、ビュー（Vue）とモデルをつなぐコールバック関数
// 登録があった場合、PJTのgInfoとdInfoをビューに送る
export type SVSyncCB = (que: SVSyncQue, ...rest: any) => void;

// QA用のコールバック関数
// PJTに登録があった場合、TMがセットされるたびに呼び出される
// 複数のQA関数を動かすため、Promiseを返す
export type QAfuncCB = (dInfo: DataInfo, ...rest: any) => Promise<QaResultInfo[]>;

// Electronからビュー側を動かすための情報を送る型
export interface SVRequireQue {
    target: '' | 'message' | 'openME' | 'move' | 'restore' | 'clear backup' | 'clear all' | 'reflesh';
    data: any;
    moveto?: PageName;
}

// Vue側のページを管理する
// export type PageName = '/cat' | '/check' | '/standalone' | '/import' | '/export' | '/pref' | '/filter' | '/test';
export type PageName = '/cat' | '/standalone' | '/import' | '/export' | '/restore'|
                        '/pref' | '/prefMT' | '/dbManager' |'/filter' | '/test' | '/termhelper';

// Vue -> Main
// Vue側だけが持っている情報を送る
export interface VSSyncQue {
    target: '' | 'pref' | 'restore' | 'MT';
    data: any;
}

// VueからElectronに関数の実行を依頼する
export interface VSRequireQue {
    target: '' | 'calcSt'| 'newTt' | 'dbView' | 'onTop' | 'term-helper' | 'move-page' | 'add-wholeterm';
    data: any;
}

// Vue -> Main PJTのDatabase操作 GET
export interface VSGetDBQue {
    control: '' | 'ME' | 'filter';
    target: 'TMs' | 'TBs' | 'TMTBs';
    data: any;
}

// Vue -> Main PJTのDatabase操作 SET
export interface VSSetDBQue {
    control: '' | 'confirmValFromME' | 'deleteValFromME' | 'quickAddTermFromME' |
            'confirmTBValFromME' | 'deleteTBValFromME' |
            'adjustFid' | 'removeDB';
    target: 'TMs' | 'TBs' | 'TMTBs';
    data: any;
}

// VueのインポートウィザードからMainに送るキュー
export interface ImportQue {
    from: 'JSON' | 'TxX' | 'Excel' | 'FExcel' | 'CTSV' | 'FCTSV' | 'FExcel';
    into: 'TMs' | 'TBs';
    srcLang: string;
    tgtLang: string;
    scol: string;
    tcol: string;
    mcol: string;
    header: number;
    delimiter: ',' | '\t';
    enclosure: '"' | '\'' | '';
    mode: ReadMode;
}

// 読み込み方法を制御するための文字列
// 1文字目が Inner | External | Original
// 2文字目が Allow | Prohibit | Original
export type ReadMode = 'ia' | 'ip' | 'io' | 'oo' | 'ea' | 'ep' | 'eo';
