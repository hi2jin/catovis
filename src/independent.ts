import { createCatServer } from './core/catServer';
import { CAT_PORT } from './core/params';
import { ProjectInfo } from './core/pdb';
import Datastore from 'nedb';

const myCAT = createCatServer();
myCAT.listen(CAT_PORT);

console.log('loaded');
